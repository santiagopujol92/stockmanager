<div class="input-group">
    <span class="input-group-addon">
    	<label class="col-red">*</label> 
        <i class="material-icons">card_membership</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'msg' => 'Nombre', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>