<div class="col-sm-6">
    <label for="name">Nombre:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">person</i>
        </span>
        <div class="form-line">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => "Nombre", 'msg' => 'Nombre', 'required' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <label for="lastname">Apellido:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">person</i>
        </span>
        <div class="form-line">
            {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => "Apellido", 'msg' => 'Apellido', 'required' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <label for="name">Email :</label>
    <div class="input-group">
        <span class="input-group-addon">
            {{-- <label class="col-red">*</label>  --}}
            <i class="material-icons">email</i>
        </span>
        <div class="form-line">
            {!! Form::email('email', null, ['class' => 'form-control' , 'placeholder' => "Email", 'msg' => 'Email', 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <label for="name">Cuit:</label>
    <div class="input-group">
        <span class="input-group-addon">
            {{-- <label class="col-red">*</label>  --}}
            <i class="material-icons">description</i>
        </span>
        <div class="form-line">
            {!! Form::text('cuit', null, ['class' => 'form-control', 'placeholder' => "Cuit", 'msg' => 'Cuit','autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <label for="name">Dirección:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">store</i>
        </span>
        <div class="form-line">
            {!! Form::text('adress', null, ['class' => 'form-control', 'placeholder' => "Dirección", 'msg' => 'Dirección', '' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <label for="name">Piso:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">store</i>
        </span>
        <div class="form-line">
            {!! Form::text('floor', null, ['class' => 'form-control', 'msg' => 'Piso', 'placeholder' => "Piso", '' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <label for="name">Departamento:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">store</i>
        </span>
        <div class="form-line">
            {!! Form::text('department', null, ['class' => 'form-control', 'msg' => 'Departamento', 'placeholder' => "Departamento", '' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <label for="name">Teléfono:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">phone_android</i>
        </span>
        <div class="form-line">
            {!! Form::text('phone_1', null, ['class' => 'form-control', 'msg' => 'Teléfono 1', 'placeholder' => "Teléfono 1", '' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <label for="name">Teléfono 2:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">phone_iphone</i>
        </span>
        <div class="form-line">
            {!! Form::text('phone_2', null, ['class' => 'form-control', 'msg' => 'Teléfono 2', 'placeholder' => "Teléfono 2" , '' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <label for="name">Concición Iva:</label>
    <div class="input-group input-above">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">shopping_basket</i>
        </span>
        <div class="form-line">
            <select name="id_condicion_iva" class="form-control show-tick" required msg="Condición Iva" data-live-search="true" >
                <option value="0">Seleccione Concición Iva</option>
                @foreach ($data_condicion_iva as $reg) 
                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <label for="name">Tipo:</label>
    <div class="input-group input-above">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">person</i>
        </span>
        <div class="form-line">
            <select name="id_type_people" class="form-control show-tick" msg="Tipo" required data-live-search="true" >
                <option value="0">Seleccione Tipo</option>
                @foreach ($data_type_person as $reg) 
                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <label for="name">País:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">location_city</i>
        </span>
        <div class="form-line">
            <select name="id_country" msg="País" onchange="changeDataSelectTarget('provincias', 'findByCountryId', 'id_province', 'Provincia', this.form.id, this.value); validateNoSelectionToSelect('id_city', 'Ciudad' ,this.form.id, this.value);" class="form-control show-tick" autofocus="true"  required data-live-search="true" >
                <option value="0">Seleccione un País</option>
                @foreach ($data_countries as $reg)
                    <option value="{{$reg->id}}" >{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <label for="name">Provincia:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">location_city</i>
        </span>
        <div class="form-line">           
            <select name="id_province" msg="Provincia" onchange="changeDataSelectTarget('ciudades', 'findByProvinceId', 'id_city', 'Ciudad', this.form.id, this.value)" class="form-control show-tick" disabled="true" required data-live-search="true" >
                <option value="0">Seleccione Provincia</option>
            </select>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <label for="name">Ciudad:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">location_city</i>
        </span>
        <div class="form-line">           
            <select name="id_city" msg="Ciudad" class="form-control show-tick" disabled="true" required data-live-search="true" >
                <option value="0">Seleccione Ciudad</option>
            </select>
        </div>
    </div>
</div>
