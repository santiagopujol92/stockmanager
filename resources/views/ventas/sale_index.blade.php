@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection   
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}s</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING -->
	                     	<div class="icon-button-demo">
				                <button class="btn btn-link bg-pink waves-effect pull-right" type="button" onclick="clearForm('form{{$form}}');configSaveButtonAdd();unlockFieldsSaleOnEdit();resetValueFood();" data-toggle="modal" data-target="#modal{{$modals_btns}}">
	                                <div class="demo-google-material-icon"> 
	                                	{{-- <i class="material-icons">add_circle</i>  --}}
	                                	<span class="icon-name">NUEVA {{$titulo}}</span> 
	                               	</div>
				        		</button>
				        	</div>
			        		<h4>LISTADO DE {{$titulo}}S</h4>
	                    </div>
	                    <div class="body table-responsive">
	                        <div class="{{$modals_btns}}s">

		                        <table class="table table-bordered table-striped table-hover datatable dataTable">
		                            <thead>
		                                <tr>
		                                	<th class="text-th">Nro. Venta</th>
		                                    <th class="text-th">Persona</th>
											<th class="text-th">Precio Venta</th>
											<th class="text-th">Costo Venta</th>
											<th class="text-th">Fecha Venta</th>
											<th class="text-th">Hora Venta</th>
		                                    <th>&nbsp;&nbsp;Opciones&nbsp;&nbsp;</th>
		                                </tr>
		                            </thead>
		                            <tbody id="tbody_{{$module}}">
			                            @foreach ($data_controller as $reg) 
			                                <tr>
			                                	<td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}');">
			                                    	{{$reg->id}}
			                                    </td>
			                                    <td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}');">
			                                    	{{$reg->person_lastname}} {{$reg->person_name}}
			                                    </td>
			                                    <td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}');" class="col-teal font-bold">
			                                    	$ {{$reg->sell_price}}
			                                    </td>
			                                    <td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}');" class="col-red font-bold">
			                                    	$ {{$reg->cost_price}}
			                                    </td>
	                                    		<td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}');">
	                                    			@if ($reg->sell_date != '')
			                                    		{{Carbon\Carbon::parse($reg->sell_date)->format('d/m/Y')}}
													@else
			                                    		{{''}} 
			                                    	@endif
			                                    </td>
			                                    <td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}');">
	                                    			@if ($reg->sell_date != '')
			                                    		{{Carbon\Carbon::parse($reg->sell_date)->format('H:i')}} hs
													@else
			                                    		{{''}} 
			                                    	@endif
			                                    </td>
			                                    <td>
													<div class="icon-button-demo">
														<button type="button" href="#" data-toggle="modal" title="Ver / Editar" onclick="prepareModalDataEdit({{$reg->id}});" data-target="#modal{{$modals_btns}}" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
					                                    	<i class="material-icons">edit</i>
					                                   	</button>
				                    	             	<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar({{$reg->id}});" data-toggle="modal" data-target="#modalDelete{{$modals_btns}}" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
				                            				<i class="material-icons">delete</i>
				                                		</button>
													</div>
			                                    </td>
			                                </tr>
			                            @endforeach 
		                            </tbody>
		                        </table>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </div>
    </section>

 	@include('global_modals.modal_edit_add_global')
	@include('global_modals.modal_delete_global')

@endsection

@section('scripts')
    <!-- Custom Js -->
	<script src="../scripts/ventas.js"></script>
	<script src="../scripts/funciones_ventas.js"></script>
    <script src="../scripts/abm_basic.js"></script>
@endsection