<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>StockManager | Iniciar Sesión</title>
    <!-- Favicon-->
    <link rel="icon" href="assets_md/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets_md/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="assets_md/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="assets_md/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets_md/css/style.css" rel="stylesheet">
</head>

<body class="login-page bg-pink">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><b> StockManager</b><br>Tu Negocio de Comida</a>
            <small>By Santiago Pujol</small>
        </div>
        <div class="card">
            <div class="body">
                {!!Form::open(['id' => 'formLogin', 'route' => 'login', 'method' => 'POST'])!!}
                    {{ csrf_field() }}
                    <div class="msg">Ingrese email y contraseña para iniciar sesión</div>
                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="email" placeholder="Email" required autofocus>
                        </div>
                    </div>
                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control " name="password" placeholder="Contraseña" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            {!!Form::submit('INGRESAR', ['class' => 'btn btn-block bg-green waves-effect'])!!}                   
                        </div>
                    </div>
                {!!Form::close()!!}
            </div>
        </div>
        @if (Session::has('message-error'))
            <div class="row clearfix jsdemo-notification-button">
                <button type="button" class="btn btn-danger btn-block waves-effect" data-placement-from="bottom" data-placement-align="left"
                        data-animate-enter="" data-animate-exit="" data-color-name="alert-danger">
                    {{Session::get('message-error')}}
                </button>
            </div>
        @endif
    </div>

    <!-- Jquery Core Js -->
    <script src="assets_md/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets_md/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="assets_md/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="assets_md/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="assets_md/js/admin.js"></script>
    <script src="assets_md/js/pages/examples/sign-in.js"></script>
</body>

</html>