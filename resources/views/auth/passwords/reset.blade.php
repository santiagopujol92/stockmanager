<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Forgot Password | SysPamp - La Juanita</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets_md/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets_md/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets_md/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets_md/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets_md/css/style.css" rel="stylesheet">
</head>

<body class="fp-page">
    <div class="fp-box">
        <div class="logo">
            <a href="javascript:void(0);">Admin <b> - La Juanita </b> Reset Password </a>
            <small>By SysPamp</small>
        </div>
        <div class="card">
            <div class="body">
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                   {{--  {!!Form::open(['id' => 'formResetPassword', 'route' => 'password.request', 'method' => 'POST'])!!} --}}
                    <form id="formResetPassword" role="form" action="{{ url('/password/reset') }}" method="POST" >
                        {{ csrf_field() }}
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <span class="input-group-addon">
                                <i class="material-icons">email</i>
                            </span>
                            <div class="form-line">
                                <input type="email" value="{{ $email or old('email') }}" class="form-control" name="email" placeholder="Email" required autofocus>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block align-center">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                        <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                             <div class="form-line">
                                <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                        <div class="input-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif

                        <button type="submit" class="btn btn-block btn-lg bg-pink waves-effect">Reset Password</button>
                        <div class="row m-t-20 m-b--5 align-center">
                            <a href="/">Sign In!</a>
                        </div>
                    </form>
                    {{-- {!!Form::close()!!} --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Jquery Core Js -->
    <script src="/assets_md/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/assets_md/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/assets_md/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="/assets_md/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="/assets_md/js/admin.js"></script>
    <script src="/assets_md/js/pages/examples/forgot-password.js"></script>

    <script src="/scripts/funciones.js"></script>

</body>
</html>
