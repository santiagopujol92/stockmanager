<label for="description">Nombre:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">description</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'msg' => 'Nombre', 'autofocus' => true]) !!}
    </div>
</div>

<label for="type">Tipo Medición:</label>
<div class="input-group input-above">
    <span class="input-group-addon">
        <label class="col-red">*</label>
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        <select name="type" class="form-control show-tick" msg="Tipo Medición" required data-live-search="true" >
            <option value="0">Seleccione Tipo de Medición Producto</option>
            @foreach ($data_type_products as $reg)
                <option value="{{$reg->id}}" >{{$reg->description}}</option>
            @endforeach
        </select>
    </div>
</div>

<label for="value_type">Cantidad Total Compra:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">add_shopping_cart</i>
    </span>
    <div class="form-line">
        {!! Form::text('value_type', null, ['class' => 'form-control', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Cantidad Total Compra " . $modulo_msg, 'required' => true, 'msg' => 'Cantidad Total Compra', 'autofocus' => true]) !!}
    </div>
</div>

<label for="real_purchase_price">Precio Total Compra:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">attach_money</i>
    </span>
    <div class="form-line">
        {!! Form::text('real_purchase_price', null, ['class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Precio Total Compra " . $modulo_msg, 'required' => true, 'msg' => 'Precio Total Compra', 'autofocus' => true]) !!}
    </div>
</div>

<label for="last_purchase_price">Último Precio Total Compra:</label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">attach_money</i>
    </span>
    <div class="form-line">
        {!! Form::text('last_purchase_price', 0, ['class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Último Precio Total Compra " . $modulo_msg, 'autofocus' => true]) !!}
    </div>
</div>

<label for="quantity_default_value">Cantidad Por Defecto de Uso:</label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">add_shopping_cart</i>
    </span>
    <div class="form-line">
        {!! Form::text('quantity_default_value', 1, ['class' => 'form-control', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Cantidad Por Defecto de Uso", 'autofocus' => true]) !!}
    </div>
</div>

<label for="accumulator_value">Valor de Acumulación de Cantidad:</label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        {!! Form::text('accumulator_value', 1, ['class' => 'form-control', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Valor de Acumulación de Cantidad " . $modulo_msg, 'autofocus' => true]) !!}
    </div>
</div>

<label for="stock" class="col-red">Stock Disponible:</label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        {!! Form::text('stock', 0, ['class' => 'form-control col-teal font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Stock Disponible " . $modulo_msg, 'autofocus' => true]) !!}
    </div>
</div>
