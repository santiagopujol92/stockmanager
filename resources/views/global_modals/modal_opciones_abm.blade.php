{{-- MODAL OPCIONES --}}
{{-- <div class="modal fade" id="modal_opciones" tabindex="-1" role="dialog"> --}}
{{--     <div class="modal-dialog modal-sm modal-center" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" align="center">Opciones</h4>
            </div>

            <div class="modal-footer">
                <button type="button" href="#" id="btn_modal_opc_edit_{{$modals_btns}}" data-toggle="modal" title="Ver / Editar"  class="btn btn-link btn-block col-white bg-blue waves-effect">
                	<i class="material-icons">edit</i>
                	VER / EDITAR
                </button>
                <br>
                <button type="button" style="margin-top:6px;" id="btn_modal_opc_delete_{{$modals_btns}}" href="#" title="Eliminar"  data-toggle="modal" class="btn btn-link btn-block bg-red col-white waves-effect">
                	<i class="material-icons">delete</i>
                	ELIMINAR
                </button>
                <br>
                <button type="button" style="margin-top:6px;" class="btn btn-link btn-block bg-grey waves-effect" >
                	<i class="material-icons">close</i>
                	CERRAR
            	</button>
            </div>
        </div>
    </div> --}}
{{-- </div> --}}
{{-- # MODAL OPCIONES  --}}

{{--  DIALOG OPCIONES 2  --}}
<button type="button" id="executeOpcionesModal" data-toggle="modal" data-target="#modal_opciones"></button>
<div class="modal fade" id="modal_opciones" tabindex="0" role="dialog">
    <div class="sweet-overlay" tabindex="-1" style="opacity: 1.09; display: block;"></div>
    <div role="document">
        <div class="modal-content">
            <div class="sweet-alert showSweetAlert visible" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
                <h4>Opciones</h4>
                <div class="sa-button-container" id="modal_opciones_content">
                    <button type="button" href="#" id="btn_modal_opc_unlock_{{$modals_btns}}" data-toggle="modal" title="Liberar Mesa"  class="btn btn-link btn-block col-white bg-green waves-effect button-opciones">
                        <i class="material-icons">lock_open</i>
                        <b>LIBERAR MESA</b>
                    </button>
                    <button type="button" href="#" id="btn_modal_opc_lock_{{$modals_btns}}" data-toggle="modal" title="Ocupar Mesa"  class="btn btn-link btn-block col-white bg-red waves-effect button-opciones">
                        <i class="material-icons">lock_outline</i>
                        <b>OCUPAR MESA</b>
                    </button>
                    <button type="button" href="#" id="btn_modal_opc_reserve_{{$modals_btns}}" data-toggle="modal" title="Reservar Mesa"  class="btn btn-link btn-block col-white bg-orange waves-effect button-opciones">
                        <i class="material-icons">phone_forwarded</i>
                        <b>RESERVAR MESA</b>
                    </button>
                    <div id="modal_opciones_separar"></div>
                    <button type="button" href="#" id="btn_modal_opc_edit_{{$modals_btns}}" data-toggle="modal" title="Ver / Editar"  class="btn btn-link btn-block col-white bg-blue waves-effect button-opciones">
                        <i class="material-icons">edit</i>
                        <b>VER / EDITAR</b>
                    </button>
                    <button type="button" href="#" id="btn_modal_opc_add_{{$modals_btns}}" data-toggle="modal" title="Ver / Editar"  class="btn btn-link btn-block col-white bg-green waves-effect button-opciones">
                        <i class="material-icons">add</i>
                        <b>AUMENTAR STOCK</b>
                    </button>
                    <button type="button" id="btn_modal_opc_delete_{{$modals_btns}}" href="#" title="Eliminar"  data-toggle="modal" class="btn btn-link btn-block bg-red col-white waves-effect button-opciones">
                        <i class="material-icons">delete</i>
                        <b>ELIMINAR</b>
                    </button>
                    <button type="button" class="btn btn-link btn-block bg-grey waves-effect button-opciones" data-dismiss="modal" >
                        <i class="material-icons">close</i>
                        <b>CERRAR</b>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- # DIALOG OPCIONES 2  --}}