<!-- MODAL ADD / EDIT -->   
    <div class="modal fade scrollable" id="modal{{$modals_btns}}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-center" id="modal_size_class" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{$titulo}}</h4>
                </div>
                {!! Form::open(['id' => "form" . $form, 'method' => 'POST']) !!}
                    <div class="modal-body">
                            @include($module . '.forms.form_' . $name_file . '')
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                        {!! Form::button('GUARDAR', ['name' => "btnguardar" . $modals_btns, 'onclick' => 'agregar(true)', 'class' => 'btn btn-link bg-green waves-effect']) !!}
                    </div>
                {!! Form::close()!!}
                <!-- LOADING -->
                <div id="loading_modal"></div>
                <!-- FIN LOADING -->
                <div id="modal_mensaje"></div>
            </div>
        </div>
    </div>
<!-- # MODAL ADD / EDIT  -->