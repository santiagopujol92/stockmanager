@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection     

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}</h3>
            </div>
            <div class="row clearfix">

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>LISTADO DE COMANDAS DE BEBIDAS</h2>
                        </div>
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active" style="margin-left:2px;">
                                <a href="#preparar" data-toggle="tab">COMANDAS A PREPARAR</a>
                            </li>
                            <li role="presentation">
                                <a href="#retirar" data-toggle="tab">COMANDAS A RETIRAR</a>
                            </li>
                            <li role="presentation">
                                <a href="#entregados" data-toggle="tab">COMANDAS ENTREGADAS</a>
                            </li>
                            <li role="presentation">
                                <a href="#cancelados" data-toggle="tab">COMANDAS CANCELADAS</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in animated fadeInRight" id="preparar">
                                <div class="header">
                                    <h2>COMANDAS A PREPARAR</h2>
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>Mesa</th>
                                                    <th>Producto/s</th>
                                                    <th>Fecha y Hora</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_{{$module}}_a_preparar">
                                                @foreach ($data_tables_with_orders_1 as $reg) 
                                                    <tr>
                                                        <td>{{$reg->nro_mesa}}</td>
                                                        <td>
                                                            @foreach ($data_tables_with_orders_products_1 as $regint) 
                                                                @if ($reg->id_tabla == $regint->id_tabla && $reg->created_at == $regint->created_at)
                                                                    {{$regint->nombre_producto}} ({{$regint->cantidad}})
                                                                @else
                                                                    @continue
                                                                @endif
                                                            @endforeach 
                                                        </td>
                                                        <td>{{Carbon\Carbon::parse($reg->created_at)->format('d/m/Y H:i:s')}} hs</td>
                                                        <td>
                                                            <div class="icon-button-demo">
                                                                <button type="button" href="#" data-toggle="modal" title="Ver / Editar" onclick="prepareModalDataEdit({{$reg->id}});" data-target="#modal{{$modals_btns}}" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
                                                                    <i class="material-icons">edit</i>
                                                                </button>
                                                                <button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar({{$reg->id}});" data-toggle="modal" data-target="#modalDelete{{$modals_btns}}" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                                                                    <i class="material-icons">delete</i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade animated fadeInRight" id="retirar">
                                <div class="header">
                                    <h2>COMANDAS A RETIRAR</h2>
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>Mesa</th>
                                                    <th>Producto/s</th>
                                                    <th>Fecha/s</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_{{$module}}_a_retirar">
                                                @foreach ($data_tables_with_orders_2 as $reg) 
                                                    <tr>
                                                        <td>{{$reg->nro_mesa}}</td>
                                                        <td>
                                                            @foreach ($data_tables_with_orders_products_2 as $regint) 
                                                                @if ($reg->id_tabla == $regint->id_tabla && $reg->created_at == $regint->created_at)
                                                                    {{$regint->nombre_producto}} ({{$regint->cantidad}})
                                                                @else
                                                                    @continue
                                                                @endif
                                                            @endforeach 
                                                        </td>
                                                        <td>{{Carbon\Carbon::parse($reg->created_at)->format('d/m/Y H:i:s')}} hs</td>
                                                    </tr>
                                                @endforeach 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade animated fadeInRight" id="entregados">
                                <div class="header">
                                    <h2>COMANDAS ENTREGADAS</h2>
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>Mesa</th>
                                                    <th>Producto/s</th>
                                                    <th>Fecha/s</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_{{$module}}_entregadas">
                                                @foreach ($data_tables_with_orders_3 as $reg) 
                                                    <tr>
                                                        <td>{{$reg->nro_mesa}}</td>
                                                        <td>
                                                            @foreach ($data_tables_with_orders_products_3 as $regint) 
                                                                @if ($reg->id_tabla == $regint->id_tabla && $reg->created_at == $regint->created_at)
                                                                    {{$regint->nombre_producto}} ({{$regint->cantidad}})
                                                                @else
                                                                    @continue
                                                                @endif
                                                            @endforeach 
                                                        </td>
                                                        <td>{{Carbon\Carbon::parse($reg->created_at)->format('d/m/Y H:i:s')}} hs</td>
                                                    </tr>
                                                @endforeach 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade animated fadeInRight" id="cancelados">
                                <div class="header">
                                    <h2>COMANDAS CANCELADOS</h2>
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>Mesa</th>
                                                    <th>Producto/s</th>
                                                    <th>Fecha/s</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_{{$module}}_canceladas">
                                                @foreach ($data_tables_with_orders_4 as $reg) 
                                                    <tr>
                                                        <td>{{$reg->nro_mesa}}</td>
                                                        <td>
                                                            @foreach ($data_tables_with_orders_products_4 as $regint) 
                                                                @if ($reg->id_tabla == $regint->id_tabla && $reg->created_at == $regint->created_at)
                                                                    {{$regint->nombre_producto}} ({{$regint->cantidad}})
                                                                @else
                                                                    @continue
                                                                @endif
                                                            @endforeach 
                                                        </td>
                                                        <td>{{Carbon\Carbon::parse($reg->created_at)->format('d/m/Y H:i:s')}} hs</td>
                                                    </tr>
                                                @endforeach 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
    </section>

    @include('global_modals.modal_opciones_abm')
    @include('global_modals.modal_edit_add_global')
    @include('global_modals.modal_delete_global')

@endsection   

<!-- Scripts Only For This Page -->
@section('scripts')
    <!-- Custom Js -->
    {{-- <script src="../scripts/bar.js"></script> --}}
    <script src="../scripts/{{$module}}.js"></script>
    <script src="../scripts/abm_basic.js"></script>
@endsection     