
<label for="description">Nro. Mesa:</label>
<div class="input-group">
    <span class="input-group-addon">
    	<label class="col-red">*</label> 
        <i class="material-icons">restaurant</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'msg' => 'Nro Mesa', 'placeholder' => "Ingrese Nro. " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>

<label for="id_status_table">Estado:</label>
<div class="input-group input-above3">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">compare_arrows</i>
    </span>
    <div class="form-line">
        <select name="id_status_table" class="form-control show-tick" onchange="changeSelectSaleByValue(this.form.id, this.value, 'id_sale')" msg="Estado" required data-live-search="true" >
            <option value="0">Seleccione un Estado</option>
            @foreach ($data_status_tables as $reg) 
                <option value="{{$reg->id}}">{{$reg->description}}</option>
            @endforeach
        </select>
    </div>
</div>

<div id="div_select_id_sale_tables">
    <label for="id_sale">Venta:</label>
    <div class="input-group input-above2">
        <span class="input-group-addon" id="span_id_sale_by_tables">
            <i class="material-icons">assignment</i>
        </span>
        <div class="form-line">
            <select name="id_sale" class="form-control show-tick" onchange="configButtonEditSale(this.form.id, this.value, 'btn_sale_table');" msg="Seleccione una Venta" data-live-search="true" >
                <option value="0">Seleccione una Venta</option>
                @foreach ($data_sales as $reg) 
                    <option value="{{$reg->id}}">Nro: {{$reg->id}} - {{Carbon\Carbon::parse($reg->sell_date)->format('d/m/Y H:i')}} hs - {{$reg->employee_last_name}} {{$reg->employee_name}} ($ {{$reg->sell_price}})</option>
                @endforeach
            </select>
        </div>
    </div>
    <button title="Ver / Editar Venta" type="button" data-toggle="modal" data-target="#modalSale" name="btn_sale_table" class="btn btn-link waves-effect bg-cyan btn-block col-white">
        <div class="demo-google-material-icon"> 
            <span class="icon-name"> Ver / Editar Venta Seleccionada</span> 
        </div>
    </button>
    <button title="Nueva Venta" type="button" onclick="clearForm('formVenta');configSaveButtonAddVenta();unlockFieldsSaleOnEdit();resetValueFood();configNewSaleByOcuparVenta();" data-toggle="modal" data-target="#modalSale" name="btn_new_sale_table" class="btn btn-link waves-effect bg-pink col-white btn-block">
        <div class="demo-google-material-icon"> 
            <span class="icon-name"> Nueva Venta</span> 
        </div>
    </button>
</div>