@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection   
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>Administrar {{$modulo_msg}}s</h3>
            </div>
			<!-- LISTADO DE VENTAS EN MESA-->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING -->
	                     	<div class="icon-button-demo">
				                <button class="btn btn-link bg-pink waves-effect pull-right" type="button" onclick="clearForm('form{{$form}}');configSaveButtonAdd();restartConfigDefaultFormMesa()" data-toggle="modal" data-target="#modal{{$modals_btns}}">
	                                <div class="demo-google-material-icon"> 
	                                	{{-- <i class="material-icons">add_circle</i>  --}}
	                                	<span class="icon-name">NUEVA {{$titulo}}</span> 
	                               	</div>
				        		</button>
				                <button class="btn btn-link bg-pink waves-effect pull-right" type="button" onclick="clearForm('formVenta');configSaveButtonAddVenta();unlockFieldsSaleOnEdit();resetValueFood();" data-toggle="modal" data-target="#modalSale">
	                                <div class="demo-google-material-icon"> 
	                                	{{-- <i class="material-icons">add_circle</i>  --}}
	                                	<span class="icon-name">NUEVA VENTA</span> 
	                               	</div>
				        		</button>
				        	</div>
			        		<h4>LISTADO DE {{$titulo}}S</h4>
	                    </div>
	                    {{-- Mensajes de estados --}}
	                    <div class="col-md-12 align-right">
							<div class="row">	                    		
		                    	<div class="col-md-12">
			                    	Libre: 
			                    {{-- </div> --}}
			                    {{-- <div class="col-xs-6"> --}}
		                    		<span class="label bg-green">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
		                    	</div>
		                    </div>
		                    <div class="row">
		                    	<div class="col-md-12">
		                    		Ocupada: 
		                    	{{-- </div> --}}
		                    	{{-- <div class="col-xs-6"> --}}
		                    		<span class="label bg-red">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
		                    	</div>
		                    </div>
		                    <div class="row">
		                    	<div class="col-md-12">
		                    		Reservada: 
		                    	{{-- </div> --}}
		                    	{{-- <div class="col-xs-6"> --}}
		                    		<span class="label bg-orange">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
		                    	</div>
		                    </div>
		        		</div>
		   
		     		 	{{-- #Mensajes de estados --}}
	                    <div class="body table-responsive">
	                        <div class="{{$modals_btns}}s">
		                        <div id="tbody_{{$module}}">
		                            @foreach ($data_controller as $reg)
                        				<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
							                <button title="Mesa {{$reg->status}}" onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}', true, true, false, true, true);" class="btn btn-link waves-effect btn-block 	
												@if ($reg->id_status_table == 1) 
													{{'bg-red'}}
												@elseif ($reg->id_status_table == 2) 
													{{'bg-green'}}
												@elseif ($reg->id_status_table == 3) 
													{{'bg-orange'}}
												@endif" type="button">
				                                <div class="demo-google-material-icon"> 
				                                	<span class="icon-name">Mesa {{$reg->description}}</span> 
				                               	</div>
							        		</button>
		                                </div>
		                            @endforeach 
		                        </div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
            <!-- MODAL ADD / EDIT VENTA-->   
		    <div class="modal fade scrollable" style="/*z-index:1100 !important;*/" id="modalSale" tabindex="-1" role="dialog">
		        <div class="modal-dialog modal-center modal-center4 modal-lg" id="modal_size_class" role="document">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <h4 class="modal-title">VENTA</h4>
		                </div>
		                {!! Form::open(['id' => "formVenta", 'method' => 'POST']) !!}
		                    <div class="modal-body">
		                            @include('ventas' . '.forms.form_' . 'sale' . '')
		                    </div>
		                    <div class="modal-footer" >
		                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
		                        {!! Form::button('GUARDAR', ['name' => "btnguardar" . 'Sale', 'onclick' => 'agregarVenta(true, "Venta")', 'class' => 'btn btn-link bg-green waves-effect']) !!}
		                    </div>
		                {!! Form::close()!!}
		                <!-- LOADING -->
		                <div id="loading_modal"></div>
		                <!-- FIN LOADING -->
		                <div id="modal_mensaje"></div>
		            </div>
		        </div>
		    </div>
		<!-- # MODAL ADD / EDIT  VENTA-->
		<!-- MODAL OCUPAR MESA -->
	    <div class="modal fade scrollable" id="modalOcuparMesa" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-center" id="modal_size_class" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">OCUPAR MESA</h4>
                </div>
                {!! Form::open(['id' => "formOcuparMesa", 'method' => 'POST']) !!}
                    <div class="modal-body">
						<label for="id_sale">Venta:</label>
						<div class="input-group input-above2">
						    <span class="input-group-addon">
						    	<label class="col-red">*</label> 
						        <i class="material-icons">person</i>
						    </span>
						    <div class="form-line">
						        <select name="id_sale" class="form-control show-tick" msg="Venta" onchange="configButtonEditSale(this.form.id, this.value, 'btn_sale_table');" required data-live-search="true" >
						            <option value="0">Seleccione una Venta</option>
						            @foreach ($data_sales as $reg) 
						                <option value="{{$reg->id}}">Nro: {{$reg->id}} - {{Carbon\Carbon::parse($reg->sell_date)->format('d/m/Y H:i')}} hs - {{$reg->employee_last_name}} {{$reg->employee_name}} ($ {{$reg->sell_price}})</option>
						            @endforeach
						        </select>
						    </div>
						</div>
		            <button title="Ver / Editar Venta" type="button" data-toggle="modal" data-target="#modalSale" name="btn_sale_table" class="btn btn-link waves-effect bg-cyan col-white btn-block">
				        <div class="demo-google-material-icon"> 
				            <span class="icon-name"> Ver / Editar Venta Seleccionada</span> 
				        </div>
				    </button>
		            <button title="Nueva Venta" type="button" onclick="clearForm('formVenta');configSaveButtonAddVenta();unlockFieldsSaleOnEdit();resetValueFood();configNewSaleByOcuparVenta();" data-toggle="modal" data-target="#modalSale" name="btn_new_sale_table" class="btn btn-link waves-effect bg-pink col-white btn-block">
				        <div class="demo-google-material-icon"> 
				            <span class="icon-name"> Nueva Venta</span> 
				        </div>
				    </button>
                    </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" onclick="$('#modal_opciones').modal('hide');">CERRAR</button>
                        {!! Form::button('GUARDAR VENTA EN MESA', ['name' => "btnguardarOcuparMesa", 'class' => 'btn btn-link bg-green waves-effect']) !!}
                    </div>
                {!! Form::close()!!}
                <!-- MENSAJE -->
                <div id="modal_mensaje_ocupar_mesa"></div>
                {{-- LOADING --}}
                <div id="loading_ocupar_mesa"></div>
            </div>
        </div>
    </div>
	<!-- # MODAL OCUPAR MESA -->
        </div>
    </div>
    </section>

	@include('global_modals.modal_opciones_abm')
 	@include('global_modals.modal_edit_add_global')
	@include('global_modals.modal_delete_global')

@endsection

@section('scripts')
    <!-- Custom Js -->
	<script src="../scripts/{{$module}}.js"></script>
	<script src="../scripts/funciones_ventas.js"></script>
    <script src="../scripts/abm_basic.js"></script>
@endsection