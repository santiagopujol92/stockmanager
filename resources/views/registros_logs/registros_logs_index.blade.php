@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection   
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}es</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING -->
			        		<h4>LISTADO DE {{$titulo}}ES</h4>
	                    </div>
						<div class="body table-responsive">
	                        <div class="{{$modals_btns}}s">
		                        <table class="table table-bordered table-striped table-hover datatable dataTable">
		                            <thead>
		                                <tr>
		                                    <th class="text-th">Fecha</th>
		                                    <th class="text-th">Usuario</th>
		                                    <th class="text-th">Tipo Usuario</th>
		                                    <th class="text-th">Acción</th>
		                                    <th class="text-th">Entidad</th>
		                                    <th class="text-th">Url</th>
		                                    <th class="text-th">Ip</th>
		                                    <th class="text-th">Terminal</th>
		                                </tr>
		                            </thead>
		                            <tbody id="tbody_{{$module}}">
			                            @foreach ($data_controller as $reg) 
			                                <tr>
			                                	<td>@if ($reg->created_at != '')
			                                    		{{Carbon\Carbon::parse($reg->created_at)->format('d/m/Y H:i:s')}}
													@else
			                                    		{{''}} 
			                                    	@endif
			                                    </td>
			                                    <td>{{$reg->user_lastname}} {{$reg->user_name}}</td>
			                                    <td>{{$reg->type_user}}</td>
		                                     	<td>{{$reg->event}}</td>
		                                     	<td>{{$reg->auditable_type}}</td>
		                                     	{{-- <td>{{$reg->old_values}}</td> --}}
		                                     	{{-- <td>{{$reg->new_values}}</td> --}}
		                                     	<td>{{$reg->url}}</td>
		                                     	<td>{{$reg->ip_address}}</td>
												<td>{{$reg->user_agent}}</td>
			                                </tr>
			                            @endforeach 
		                            </tbody>
		                        </table>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </section>
@endsection

@section('scripts')
	<script src="/scripts/registros_logs.js"></script>
@endsection