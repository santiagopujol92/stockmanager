@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection    

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}s</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING2 -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING2 -->
	                     	<div class="icon-button-demo">
				                <button class="btn btn-link bg-pink waves-effect pull-right" type="button" onclick="clearForm('form{{$form}}');restartConfigForm();" data-toggle="modal" data-target="#modal{{$modals_btns}}">
	                                <div class="demo-google-material-icon"> 
	                                	{{-- <i class="material-icons">add_circle</i>  --}}
	                                	<span class="icon-name">NUEVO {{$titulo}}</span> 
	                               	</div>
				        		</button>
				        	</div>
			        		<h4>LISTADO DE {{$titulo}}S</h4>
	                    </div>
	                    <div class="body table-responsive">
	                         <div class="{{$modals_btns}}s">
		                        <table class="table table-bordered table-striped table-hover datatable dataTable">
		                            <thead>
		                                <tr>
		                                    <th class="text-th">Nombre</th>
		                                    <th class="text-th">Apellido</th>
		                                    <th class="text-th">Email</th>
		                                    <th class="text-th">Tipo</th>
		                                    <th class="text-th">Estado</th>
		                                    <th class="text-th">Creación</th>
		                                    <th>&nbsp;&nbsp;Opciones&nbsp;&nbsp;</th>
		                                </tr>
		                            </thead>
		                            <tbody id="tbody_{{$module}}">
		                            <!-- RECORRO DATOS QUE TRAE DEL CONTROLADOR -->
			                            @foreach ($users as $reg) 
			                                <tr>
			                                    <td onclick="abrirModalOpcionesUsuario('{{$reg->id}}', '{{$modals_btns}}');">{{$reg->name}}</td>
			                                    <td onclick="abrirModalOpcionesUsuario('{{$reg->id}}', '{{$modals_btns}}');">{{$reg->lastname}}</td>
			                                    <td onclick="abrirModalOpcionesUsuario('{{$reg->id}}', '{{$modals_btns}}');">{{$reg->email}}</td>
			                                    <td onclick="abrirModalOpcionesUsuario('{{$reg->id}}', '{{$modals_btns}}');">{{$reg->type_description}}</td>
			                                    <td onclick="abrirModalOpcionesUsuario('{{$reg->id}}', '{{$modals_btns}}');">{{$reg->status}}</td>
			                                    <td onclick="abrirModalOpcionesUsuario('{{$reg->id}}', '{{$modals_btns}}');">@if ($reg->created_at != '')
			                                    		{{Carbon\Carbon::parse($reg->created_at)->format('d/m/Y H:i:s')}}
													@else
			                                    		{{''}} 
			                                    	@endif
			                                    </td>
			                                    <td>
													<div class="icon-button-demo">
														<button type="button" href="#" data-toggle="modal" title="Ver / Editar" onclick="mostrarUsuario({{$reg->id}});" data-target="#modal{{$modals_btns}}" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
					                                    	<i class="material-icons">edit</i>
					                                   	</button>
		                    	             				<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminarUsuario({{$reg->id}});" data-toggle="modal" data-target="#modalDelete{{$modals_btns}}" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
				                            				<i class="material-icons">delete</i>
				                                		</button>
													</div>
			                                    </td>
			                                </tr>
			                            @endforeach 
		                            <!-- FIN RECORRIDO -->
		                            </tbody>
		                        </table>
		                        <!-- PAGINADOR -->
		                        {{-- {!!$users->render()!!} --}}
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </div>
    </section>
    
   	 @include('global_modals.modal_opciones_abm')
  	 @include('global_modals.modal_edit_add_global')
	 @include('global_modals.modal_delete_global')

@endsection

<!-- Scripts Only For This Page -->
@section('scripts')
	<!-- Custom Js -->
	<script src="../scripts/usuarios.js"></script>
@endsection    