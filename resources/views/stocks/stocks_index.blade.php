@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection   
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}s</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING -->
	                     	<div class="icon-button-demo">
				                <a class="btn btn-link bg-light-green waves-effect pull-right" href="productos" type="button">
	                                <div class="demo-google-material-icon"> 
	                                	<i class="material-icons">{{-- shopping_basket --}}</i> 
	                                	<span class="icon-name">IR A PRODUCTOS</span> 
	                               	</div>
				        		</a>
				        	</div>
			        		<h4>LISTADO DE {{$titulo}}S</h4>
	                    </div>
						<div class="body table-responsive">
	                        <div class="{{$modals_btns}}s">
		                        <table class="table table-bordered table-striped table-hover datatable dataTable">
		                            <thead>
		                                <tr>
		                                    <th class="text-th">Producto</th>
		                                    <th class="col-red text-th">Stock Disponible</th>
		                                    <th class="text-th">Última Carga</th>
		                                    <th>Opciones</th>
		                                </tr>
		                            </thead>
		                            <tbody id="tbody_{{$module}}">
			                            @foreach ($data_controller as $reg)
			                            	@php 
			                            		$bg = 'bg-green';
			                            		$status_stock = '(Alto Stock)';
				                            	if ($reg->stock == '0'){
													$bg = 'bg-red';
													$status_stock = '(Sin Stock)';
				                            	}else{
													// Para Gramos y Unidades
													if (($reg->type == 39 && $reg->stock < 1000) || ($reg->type == 40 && $reg->stock < 10)){
														$bg = 'bg-orange';
														$status_stock = '(Bajo Stock)';
													}
				                            	}
			                            	@endphp
			                                <tr>
			                                    <td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}', false, false, true);">
			                                    	{{$reg->description}}
			                                    </td>
			                                    <td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}', false, false, true);" class="{{$bg}}">
			                                    	<b>{{$reg->stock}} {{$reg->type_description}}</b> {{$status_stock}}
			                                    </td>
                                                <td onclick="abrirModalOpciones('{{$reg->id}}', '{{$modals_btns}}', false, false, true);">
                                                	@if ($reg->last_change_stock == null || $reg->last_change_stock == '0000-00-00 00:00:00')
                                                        {{'-'}} 
                                                    @else
                                                        {{Carbon\Carbon::parse($reg->last_change_stock)->format('d/m/Y H:i:s')}}
                                                    @endif
                                                </td>
			                                    <td>
													<div class="icon-button-demo">
														<button type="button" href="#" data-toggle="modal" title="Aumentar Stock" onclick="prepareModalDataEdit({{$reg->id}});" data-target="#modal{{$modals_btns}}" class="btn bg-green btn-circle waves-effect waves-circle waves-float">
					                                    	<i class="material-icons">add</i>
					                                   	</button>
													</div>
			                                    </td>
			                                </tr>
			                            @endforeach 
		                            </tbody>
		                        </table>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </section>
@endsection

   	 @include('global_modals.modal_opciones_abm')
 	 @include('global_modals.modal_edit_add_global')
	 @include('global_modals.modal_delete_global')

@section('scripts')
	<script src="../scripts/stocks.js"></script>
	<script src="../scripts/abm_basic.js"></script>
@endsection