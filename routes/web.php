<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* RUTEO FRENTE */
Route::get('home', 'HomeController@index');

/* RUTEO LOG */
Route::resource('log', 'LogController');
Route::resource('/', 'LogController');
Route::get('logout', 'LogController@logout');

/* RUTEOS MODULOS ABM */
Route::resource('usuarios', 'UserController');
Route::resource('tipo_usuarios', 'TypeUserController');
Route::resource('tipo_productos', 'TypeProductController');
Route::resource('productos', 'ProductoController');
Route::resource('condiciones_ivas', 'CondicionIvaController');
Route::resource('tipo_personas', 'TypePersonController');
Route::resource('personas', 'PersonController');
Route::resource('registros_logs', 'AuditController');
Route::resource('paises', 'CountryController');
Route::resource('provincias', 'ProvinceController');
Route::resource('ciudades', 'CityController');
Route::resource('comidas', 'FoodController');
Route::resource('stocks', 'StockController');
Route::resource('ventas', 'SaleController');
Route::resource('mesas', 'TableController');
Route::resource('estados_mesas', 'StatusTablesController');
Route::resource('bar', 'BarController');

/* RUTEOS DE PETICIONES DE DATOS PARA LLAMADAS AJAX, EN EL CONTROLADOR TRAER DATOS Y LLAMAR DESDE JS RUTA */
Route::get('usuarios_listar', 'UserController@listing');
Route::get('tipo_usuarios_listar', 'TypeUserController@listing');
Route::get('tipo_productos_listar', 'TypeProductController@listing');
Route::get('productos_listar', 'ProductoController@listing');
Route::get('condiciones_ivas_listar', 'CondicionIvaController@listing');
Route::get('tipo_personas_listar', 'TypePersonController@listing');
Route::get('personas_listar', 'PersonController@listing');
Route::get('paises_listar', 'CountryController@listing');
Route::get('provincias_listar', 'ProvinceController@listing');
Route::get('ciudades_listar', 'CityController@listing');
Route::get('comidas_listar', 'FoodController@listing');
Route::get('stocks_listar', 'StockController@listing');
Route::get('ventas_listar', 'SaleController@listing');
Route::get('mesas_listar', 'TableController@listing');
Route::get('estados_mesas_listar', 'StatusTablesController@listing');
Route::get('bar_listar', 'BarController@listing');

/* RUTEOS PARA DEVOLVER DATA DE SELECTS A PARTIR DE UN VALOR DE OTRO SELECT. Funciones findBy..*/
Route::get('provincias_findByCountryId/{id}', 'ProvinceController@findByCountryId');
Route::get('ciudades_findByProvinceId/{id}', 'CityController@findByProvinceId');
Route::get('productos_findByTypeProductId/{id}', 'ProductoController@findByTypeProductId');
Route::get('personas_findByTypePersonId/{id}', 'PersonController@findByTypePersonId');
Route::get('ventas_getAllSales', 'SaleController@getAllSales');

/* RUTEOS PARA OTRA DATA DE FUNCIONES ESPECIALES*/
Route::get('home_getCountsDataForHome', 'HomeController@getCountsDataForHome');
Route::get('registros_logs_getDataAuditToNotificaction/{limit}', 'AuditController@getDataAuditToNotificaction');
Route::get('ventas_destroyFoodsBySale/{id}', 'SaleController@destroyFoodsBySale');
Route::get('mesas_createOrderByTable', 'TableController@createOrderByTable');

/*OTROS RUTEOS*/

Auth::routes();
