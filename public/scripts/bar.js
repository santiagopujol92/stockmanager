$(document).ready(function(){
    activarMenu('bar');
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Bar';
var form = 'Bar';
var module = 'bar';
var modals_btns = 'Bar';


/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){
	var titulo = 'Editar Orden';
	$("#modalBar .modal-title").html(titulo.toUpperCase());

	$.get(route, function(result){
		$("#form"+form+" select[name=id_status_order_by_table]").val(result['id_status_order_by_table']).selectpicker('refresh');

		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
	});
}

function listar(){
	var route = current_route+"_listar";

	var tabla_datos_a_preparar = $("#tbody_"+module+"_a_preparar");
	var tabla_datos_a_retirar = $("#tbody_"+module+"_a_retirar");
	var tabla_datos_entragadas = $("#tbody_"+module+"_entragadas");
	var tabla_datos_canceladas = $("#tbody_"+module+"_canceladas");

    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos_a_preparar.empty();
		// tabla_datos_a_retirar.empty();
		// tabla_datos_entragadas.empty();
		// tabla_datos_canceladas.empty();
		$(result).each(function(key,value){
			console.log(result);

			// Mesas y Productos a Preparar
			var arr_mesas_a_preparar = result['data_tables_with_orders_1']
			var arr_productos_mesas_a_preparar = result['data_tables_with_orders_products_1']
			var list_productos_a_preparar = "";

			$(arr_productos_mesas_a_preparar).each(function(key_producto, producto){
				if (arr_mesas_a_preparar[0].id_tabla == producto.id_tabla && arr_mesas_a_preparar[0].created_at == producto.created_at){
					list_productos_a_preparar += producto.nombre_producto + " (" + producto.cantidad + ")";
				}
			})	

			tabla_datos_a_preparar.append(
				'<tr>'
					+'<td>'+arr_mesas_a_preparar[0].nro_mesa+'</td>'
					+'<td>'+list_productos_a_preparar+'</td>'
					+'<td>'+formatDateTime(arr_mesas_a_preparar[0].created_at, 'd-m-Y h:i:s')+' hs</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">edit</i>'
                           	+'</button>'
        	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
                				+'<i class="material-icons">delete</i>'
                    		+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");

			// Mesas y Productos a retirar
			 
			// Mesas y Productos entregados
			 
			// Mesas y Productos cancelados
			

		});
	});
	statusLoading('loading_list', 0);
 }