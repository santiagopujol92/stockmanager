$(document).ready(function(){
	startDatatable('.datatable');
	activarMenu('admin', 'usuarios');
})
var token = $('meta[name="csrf-token"]').attr('content');
var current_route = window.location;

//Inicializar formulario como esta por default donde corresponda
function restartConfigForm(){
	//Titulo modal
	$(".modal-title").html('AGREGAR USUARIO');

	$("#formUsuario input[name=password]").attr('placeholder', 'Ingrese un password');
	$("#formUsuario input[name=password]").removeClass('bg-yellow');
	$("#label_password_required").show();
	$("#formUsuario input[name=confirm]").attr('placeholder', 'Ingrese un password');
	$("#formUsuario input[name=confirm]").removeClass('bg-yellow');
	$("#label_confirm_required").show();

	$("#formUsuario button[name=btnguardarUser]").attr('onclick', 'agregar(true)');
}

/* AGREGAR USUARIO */
function agregar(){
	$("#modal_mensaje").addClass('hidden');

	var route = current_route+"";
	var datos = $("#formUsuario").serialize();

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	var array_validate = checkFormShowingMsg("formUsuario");
	var msg = '';
	if (array_validate['puedeGuardar'] == false){
		msg = array_validate['msg'];
		showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
		return false;
	}

	//Validar email
	if (!isEmail($("#formUsuario input[name=email]").val())){
		showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>El email ingresado no es valido', 'error');
		return false;
	}

	//VALIDAR CONTRASEÑAS
	if ($("#formUsuario input[name=confirm]").val() == $("#formUsuario input[name=password]").val()){

		$.ajax({
			url: route,
			headers: {  'X-CSRF-TOKEN': token},
			type: 'POST', //AL ASIGNAR POST DISPARA A store DEL CONTROLADOR
			dataType: 'json',
			data:datos,
            beforeSend: function () {
             	statusLoading('loading_add', 1, 'Agregando Usuario ..');
            },
			success: function(result){
				listUsuarios();
				clearForm('formUsuario');
				statusLoading('loading_add', 0);
				$("#modalUser").modal('hide');
				showMessageModal('Usuario Agregado con Éxito.', 'Buen Trabajo!', 'bg-green', 'done');
			},
			error: function(){
				statusLoading('loading_add', 0);
				showMessageModal('No se pudo Agregar el Usuario.', 'Atención!', 'bg-yellow', 'done');
				return false;
			}
		})	
	} else{
		showMessage('modal_mensaje', 'bg-orange','<strong>Atención! </strong>Las Contraseñas deben ser iguales ', 'error');	
		return false;
	}
}

/* LISTAR USUARIOS */
function listUsuarios(){
	var route = current_route+"_listar"; //APUNTAMOS A UNA NUEVA RUTA QUE VA A OTRA NUEVA FUNCION DEL CONTROLADOR
	var tabla_datos = $("#tbody_usuarios");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){

			var funcion_opciones = "abrirModalOpciones('"+value.id+"', 'Usuario');";

			tabla_datos.append('<tr>'
									+'<td onclick="'+funcion_opciones+'">'+value.name+'</td>'
									+'<td onclick="'+funcion_opciones+'">'+value.lastname+'</td>'
									+'<td onclick="'+funcion_opciones+'">'+value.email+'</td>'
									+'<td onclick="'+funcion_opciones+'">'+value.type_description+'</td>'
									+'<td onclick="'+funcion_opciones+'">'+value.status+'</td>'
									+'<td onclick="'+funcion_opciones+'">'+formatDateTime(value.created_at, 'd-m-Y h:i:s')+'</td>'
									+'<td>'
										+'<div class="icon-button-demo">'
											+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="mostrarUsuario('+value.id+');" data-target="#modalUser" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
		                                    	+'<i class="material-icons">edit</i>'
		                                   	+'</button>'
	                    	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminarUsuario('+value.id+');" data-toggle="modal" data-target="#modalDeleteUser" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
	                            				+'<i class="material-icons">delete</i>'
	                                		+'</button>'
										+'</div>'
									+'</td>'
								+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatable('.datatable');
}

/* MOSTRAR USUARIO */
function mostrarUsuario(idUser){
	//Cerramos modal de opciones
	$("#modal_opciones").modal('hide');

	//Titulo modal
	$(".modal-title").html('EDITAR USUARIO');


	$("#modal_mensaje").addClass('hidden');
	var route = current_route+"/"+idUser+"/edit";
	statusLoading('loading_modal', 1);

	//Cambio de aspecto de campo password al levantar editar
	$("#formUsuario input[name=password]").attr('placeholder', 'Nueva Password (Opcional)');
	$("#label_password_required").hide();
	$("#formUsuario input[name=confirm]").attr('placeholder', 'Nueva Password (Opcional)');
	$("#label_confirm_required").hide();

	$.get(route, function(result){
		$("#formUsuario input[name=name]").val(result.name);
		$("#formUsuario input[name=lastname]").val(result.lastname);
		$("#formUsuario input[name=email]").val(result.email);
		$("#formUsuario input[name=password]").val("");
		$("#formUsuario input[name=confirm]").val("");

		$('#formUsuario [name=type][value="'+result.type+'"]').prop('checked', true);

		if (result.status == 'on'){
			result.status = true;
		}else{
			result.status = false;
		}

		$('#formUsuario [name=status]').prop('checked', result.status);
		$("#formUsuario button[name=btnguardarUser]").attr('onclick', 'updateUsuario('+idUser+')');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("formUsuario");	
		statusLoading('loading_modal', 0);
	});
}

/* UPDATE USUARIO */
function updateUsuario(idUser){
	var route = current_route+"/"+idUser+"";
	var datos = $("#formUsuario").serialize();

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	var array_validate = checkFormShowingMsg("formUsuario");
	var msg = '';
	if (array_validate['puedeGuardar'] == false){
		msg = array_validate['msg'];
		showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
		return false;
	}

	//Validar email
	if (!isEmail($("#formUsuario input[name=email]").val())){
		showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>El email ingresado no es valido', 'error');
		return false;
	}

	if ($("#formUsuario input[name=confirm").val() == $("#formUsuario input[name=password").val()){
		$.ajax({
			url: route,
			headers: {  'X-CSRF-TOKEN': token},
			type: 'PUT', //AL ASIGNAR PUT DISPARA A LA FUNCION update DEL CONTROLADOR
			dataType: 'json',
			data: datos,
	        beforeSend: function () {
         		statusLoading('loading_modal', 1, 'Guardando ..');
        	},
			success: function(result){
				statusLoading('loading_modal', 0);
				listUsuarios();
				$("#modalUser").modal('hide');
				if (result.mensaje_deslogeo != ''){
					showMessageModal('<b class="col-green">Usuario Modificado con Éxito.</b><br><br><b class="col-red">' + result.mensaje_deslogeo +'</b>', 'Atención!', 'bg-yellow', 'warning');
				}else{
					showMessageModal('Usuario Modificado con Éxito.', 'Buen Trabajo!', 'bg-green', 'done');
				}
			},
			error: function(result){
				statusLoading('loading_modal', 0);
				showMessageModal('No se pudo Modificar el Usuario.', 'Atención', 'bg-yellow', 'error');
				return false;
			}
		})	
	} else{
		showMessage('modal_mensaje', 'bg-orange','<strong>Atención! </strong>Las Contraseñas deben ser iguales ', 'error');	
		return false;
	}
}

/* PASAR UN ID A BOTON DELETE DENTRO DE MODAL */
function abrirModalEliminarUsuario(idUser){
	//Cerramos modal de opciones
	$("#modal_opciones").modal('hide');
	$("#btnModalConfirmDeleteUser").attr('onclick', 'eliminarUsuario('+idUser+')');
}

/* ELIMINAR USUARIO */
function eliminarUsuario(idUser){
	var route = current_route+"/"+idUser+"";

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'DELETE', //AL ASIGNAR DELETE DISPARA A destroy DEL CONTROLADOR
		dataType: 'json',
		data: idUser,
        beforeSend: function () {
		    statusLoading('loading_modal_delete', 1, 'Eliminando Usuario ..');
        },
		success: function(result){
			statusLoading('loading_modal_delete', 0);
			listUsuarios();
			$('#modalDeleteUser').modal('hide');
			showMessageModal('Usuario Eliminado con Éxito.', 'Buen Trabajo!', 'bg-green', 'done');
		},
		error: function(){
			statusLoading('loading_modal_delete', 0);
			$('#modalDeleteUser').modal('hide');
			showMessageModal('No se pudo eliminar el Usuario.', 'Atención!', 'bg-yellow', 'error');
		}
	})	
}

//ABRIR MODAL DE OPCIONES Y CONFIGURAR ONCLICK DE BOTONES Y DATA-TARGET DE MODAL
function abrirModalOpcionesUsuario(id_reg, modal_btns_name){
    $("#executeOpcionesModal").click();

    $("#btn_modal_opc_edit_"+ modal_btns_name +"").attr('onclick', 'mostrarUsuario('+id_reg+');');
    $("#btn_modal_opc_edit_"+ modal_btns_name +"").attr('data-target', '#modal'+modal_btns_name+'');

    $("#btn_modal_opc_delete_"+ modal_btns_name +"").attr('onclick', 'abrirModalEliminarUsuario('+id_reg+');');
    $("#btn_modal_opc_delete_"+ modal_btns_name +"").attr('data-target', '#modalDelete'+modal_btns_name+'');

    $("#btn_modal_opc_add_"+ modal_btns_name +"").remove();

}

/*PAGINACION AJAX*/
// $(document).on('click', '.pagination a', function(e){
// 	e.preventDefault();
// 	var page = $(this).attr('href').split('page=')[1];
// 	var route = "http://localhost:8000/usuarios";
	
// 	$.ajax({
// 		url: route,
// 		data: {page: page},
// 		type: 'GET',
// 		dataType: 'json',
// 		success: function(data){
// 			$(".users").html(data);
// 		}
// 	})
// });

