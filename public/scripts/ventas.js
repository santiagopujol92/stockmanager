$(document).ready(function(){
	startDatatableExportable('.datatable');
	startDateTimePicker('.datetimepicker');
	activarMenu('ventas');
	//Configuracion de modal edit y add
	configEditAddModal('modal-center4 modal-lg');
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Venta';
var form = 'Venta';
var module = 'ventas';
var modals_btns = 'Sale';

//Listing para mesas
function listar(){
	var route = current_route+"_listar";
	// var route = url.origin+"/ventas_listar";

	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){

			var sell_price = (value.sell_price != null) ? '$ ' + value.sell_price  : '-';
			var cost_price = (value.cost_price != null) ? '$ ' + value.cost_price  : '-';
			var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

			tabla_datos.append('<tr>'
					+'<td onclick="'+funcion_opciones+'">'+value.id+'</td>'
					+'<td onclick="'+funcion_opciones+'">'+value.person_lastname+' '+value.person_name+'</td>'
					+'<td onclick="'+funcion_opciones+'" class="col-teal font-bold">'+sell_price+'</td>'
					+'<td onclick="'+funcion_opciones+'" class="col-red font-bold">'+cost_price+'</td>'
					+'<td onclick="'+funcion_opciones+'">'+formatDateTime(value.sell_date, 'd-m-Y')+'</td>'
					+'<td onclick="'+funcion_opciones+'">'+formatDateTime(value.sell_date, 'h:i')+' hs</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">edit</i>'
                           	+'</button>'
        	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
                				+'<i class="material-icons">delete</i>'
                    		+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatableExportable('.datatable');
 }

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){

	resetValueFood();

	$.get(route, function(result){

		//Guardo los datos de venta en variable venta
		var venta = result.data_controller[0];

		//TITULO VA CON NRO DE VENTA
		var titulo = 'Editar '+modulo_msg+ ' <b style="color:brown;"> ' + venta.id + '</b>';
		$("#modal_size_class .modal-title").html(titulo.toUpperCase());

		//Guardo el array doble en variable comidas_por_venta para luego recorrerla
		var comidas_por_venta = result.data_foods_by_sale;

		//Recorro el array de datos de comidas de la venta
		for (var i = 0; i < comidas_por_venta.length; i++) {
		   	var comida = comidas_por_venta[i];
	
			//Seteo el checkbox correspondiente de comida
		   	$("#form"+form+" [type=checkbox]#check_food-"+comida.id_food).prop('checked', true);

		   	//Seteo el quantity q tiene cargado el comida
		   	$("#form"+form+" [name=quantity_food-"+comida.id_food+"]").val(comida.quantity);

		   	//Ejecuto la funcion de calculo de costo individual + acumulacion del global cada vez que seteo un input
		   	calcAndShowFoodByQuantity("form"+form, comida.quantity, 'check_food-'+comida.id_food, 'cost_price_food-'+comida.id_food, 'sell_price_food-'+comida.id_food);
		}		

		$("#form"+form+" select[name=id_person]").val(venta.id_person).selectpicker('refresh');
		$("#form"+form+" select[name=id_person_employee]").val(venta.id_person_employee).selectpicker('refresh');
		$("#form"+form+" input[name=cost_price]").val(venta.cost_price);
		$("#form"+form+" input[name=sell_price]").val(venta.sell_price);
		$("#form"+form+" input[name=sell_date]").val(formatDateTime(venta.sell_date, 'd-m-Y h:i:s'));
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Setear funciones al boton de desbloquear productos
		$("#form"+form+" #btn_unlock_foods").attr('onclick', 'unlockFieldsSaleOnEdit();deleteFoodsOfSale('+id+')');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);

		//Bloquear todo los inputs y botones de productos porque no puede ser modificado una vez hecha la venta
		lockFieldsSaleOnEdit();
		
		statusLoading('loading_modal', 0);
	});
}