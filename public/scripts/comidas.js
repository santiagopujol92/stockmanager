$(document).ready(function(){
	startDatatableExportable('.datatable');
	activarMenu('comidas');
	//Configuracion de modal edit y add
	configEditAddModal('modal-center4 modal-lg');
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Comida o Bebida';
var form = 'Comida';
var module = 'comidas';
var modals_btns = 'Food';

function listar(){
	var route = current_route+"_listar";
	
	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){

			var sell_price = (value.sell_price != null) ? '$ ' + value.sell_price  : '-';
			var cost_price = (value.cost_price != null) ? '$ ' + value.cost_price  : '-';
			var status = (value.status == 1) ? 'Disponible' : 'No Disponible';
			var col_status = (value.status == 1) ? 'bg-green' : 'bg-red';
			var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

			tabla_datos.append('<tr>'
					+'<td onclick="'+funcion_opciones+'">'+value.description+'</td>'
					+'<td class="col-green font-bold" onclick="'+funcion_opciones+'">'+sell_price+'</td>'
					+'<td class="col-red font-bold" onclick="'+funcion_opciones+'">'+cost_price+'</td>'
					+'<td onclick="'+funcion_opciones+'" class="'+col_status+'">'+status+'</td>'
					+'<td onclick="'+funcion_opciones+'">'+value.type+'</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">edit</i>'
                           	+'</button>'
        	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
                				+'<i class="material-icons">delete</i>'
                    		+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatableExportable('.datatable');
 }

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){
	resetValueProducts();
	$.get(route, function(result){

		//Guardo los datos de comida en variable comida
		var comida = result.data_controller[0];

		//Guardo el array doble en variable productos_por_comida para luego recorrerla
		var productos_por_comida = result.data_products_by_food;

		//Limpio los checks
		$("#form"+form+" [type=checkbox]").prop('checked', false);
		//Recorro el array de datos de productos de la comida
		for (var i = 0; i < productos_por_comida.length; i++) {
		   	var producto = productos_por_comida[i];
	
			//Seteo el checkbox correspondiente de producto
		   	$("#form"+form+" [type=checkbox]#check_product-"+producto.id_product).prop('checked', true);

		   	//Seteo el quantity q tiene cargado el producto
		   	$("#form"+form+" [name=quantity_product-"+producto.id_product+"]").val(producto.quantity);

		   	//Ejecuto la funcion de calculo de costo individual cada vez que seteo un input
		   	showCalcCostProductByQuantity("form"+form, producto.quantity, 'check_product-'+producto.id_product, 'cost_price_product-'+producto.id_product);
		}		

		$("#form"+form+" input[name=description]").val(comida.description);
		$("#form"+form+" select[name=status]").val(comida.status).selectpicker('refresh');
		$("#form"+form+" select[name=type]").val(comida.type).selectpicker('refresh');
		$("#form"+form+" input[name=cost_price]").val(comida.cost_price);
		$("#form"+form+" input[name=sell_price]").val(comida.sell_price);
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
	});
}

//Funcion para setear la cantidad de producto en 1 o 0 de acuerdo al check
function setQuantityProduct(formId, inputCheckSourceId, idProduct, defaultQuantity){
	sourceInput = $("#"+formId+ " #" +inputCheckSourceId);
	targetInput = $("#"+formId+" input[name=quantity_product-"+idProduct+"]");

	if (defaultQuantity == '' || defaultQuantity == 0 || defaultQuantity == null){
		defaultQuantity == 1;
	}

	if(sourceInput.prop('checked') == true){
		targetInput.val(defaultQuantity);
	}else{
		targetInput.val(0);
	}

	var quantityProduct = $("#"+formId+" input[name=quantity_product-"+idProduct+"]").val();
	showCalcCostProductByQuantity(formId, quantityProduct, 'check_product-'+idProduct, 'cost_price_product-'+idProduct);
}

//Funcion para sumar o restar cantidad de acuerdo al boton + o -
function addRemoveQuantityProduct(formId, idProduct, value){
	targetInput = $("#"+formId+" input[name=quantity_product-"+idProduct+"]");

	if (targetInput.val() >= 1 ){
		//Si el valor del input es 1 y el value -1 no deja restar mas
		if (targetInput.val() == 1 && value == -1){
			return false;
		}
		//Calculo
		targetInput.val(parseInt(targetInput.val()) + parseInt(value));

		var quantityProduct = $("#"+formId+" input[name=quantity_product-"+idProduct+"]").val();
		showCalcCostProductByQuantity(formId, quantityProduct, 'check_product-'+idProduct, 'cost_price_product-'+idProduct);
	}
}

//Calculo de gasto con tipo de medicion y valor dinamico
function showCalcCostProductByQuantity(formId, valueQuantity, checkInputName, labelCostProductId){
	var checkInputProduct = $("#"+formId+" input[name="+checkInputName+"]");
	var labelCostProduct = $("#"+formId+" #"+labelCostProductId);

	var quantity_purchase = checkInputProduct.attr('quantity_purchase');
	var purchase_price_by_quantity = checkInputProduct.attr('purchase_price_by_quantity');

	//Calcular el costo de acuerdo al precio compra y cantidad compra con la cantidad ingresa
	var costProductByQuantity = 0;
	if(purchase_price_by_quantity > 0 && quantity_purchase > 0 && valueQuantity > 0){
		costProductByQuantity = (parseFloat(valueQuantity) * parseFloat(purchase_price_by_quantity)) / parseFloat(quantity_purchase);		
		costProductByQuantity = costProductByQuantity.toFixed(2);
	}

	labelCostProduct.html('$ ' + costProductByQuantity);
	labelCostProduct.attr('cost-product', costProductByQuantity);

	//Seteando el valor de attr de los labels de cost individual al total general de costo
	var inputCostoTotal = $("#"+formId+" input[name=cost_price]");
	var costoTotal = 0;

	//Busco todos los labels con class cost-product
	//y al attr cost-product lo guardo
	$("#"+formId+ " [cost-product]").each(function(index, el) {
		costoTotal = parseFloat(costoTotal) + parseFloat($(el).attr('cost-product'));
	});
	
	inputCostoTotal.val(costoTotal.toFixed(2));

}

//Inicializo a 0 los valores de impacto en producto para que no haya conflicots
function resetValueProducts(){
	// Limpio los checks
	$("#form"+form+" [type=checkbox]").prop('checked', false);
	//Limpiar valores
	$("#form"+form+" .quantityInputProduct").val(0);
	$("#form"+form+" label[cost-product]").attr('cost-product', '0').html('$ 0.00');
}
