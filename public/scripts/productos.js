$(document).ready(function(){
	startDatatableExportable('.datatable');
	activarMenu('productos');
	configEditAddModal('modal-center4 modal-lg');
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Producto';
var form = 'Producto';
var module = 'productos';
var modals_btns = 'Product';

function listar(){
	var route = current_route+"_listar";
	
	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){

			var quantity_default_value = (value.quantity_default_value != null) ? value.quantity_default_value + ' ' + value.type_description : '-';
			var last_purchase_price = (value.last_purchase_price != null) ? '$ ' + value.last_purchase_price : '-';
			var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

			tabla_datos.append('<tr>'
					+'<td onclick="'+funcion_opciones+'">'+value.description+'</td>'
					+'<td onclick="'+funcion_opciones+'">'+value.value_type+' '+value.type_description+'</td>'
					+'<td class="font-bold" onclick="'+funcion_opciones+'">$ '+value.real_purchase_price+'</td>'
					+'<td class="font-bold" onclick="'+funcion_opciones+'">'+last_purchase_price+'</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">edit</i>'
                           	+'</button>'
        	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
                				+'<i class="material-icons">delete</i>'
                    		+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatableExportable('.datatable');
 }

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){

	$.get(route, function(result){
		$("#form"+form+" input[name=description]").val(result.description);
		$("#form"+form+" select[name=type]").val(result.type).selectpicker('refresh');
		$("#form"+form+" input[name=value_type]").val(result.value_type);
		$("#form"+form+" input[name=real_purchase_price]").val(result.real_purchase_price);
		$("#form"+form+" input[name=last_purchase_price]").val(result.last_purchase_price);
		$("#form"+form+" input[name=quantity_default_value]").val(result.quantity_default_value);
		$("#form"+form+" input[name=accumulator_value]").val(result.accumulator_value);
		$("#form"+form+" input[name=stock]").val(result.stock);
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
	});
}