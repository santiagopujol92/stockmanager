<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderByTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_by_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_status_order_by_table')->unsigned();
            $table->foreign('id_status_order_by_table')->references('id')->on('status_order_by_tables');
            $table->integer('id_table')->unsigned();
            $table->foreign('id_table')->references('id')->on('tables');
            $table->integer('id_sale')->unsigned();
            $table->foreign('id_sale')->references('id')->on('sales');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_by_tables');
    }
}
