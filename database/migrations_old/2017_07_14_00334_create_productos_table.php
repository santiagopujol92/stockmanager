<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 150);
            $table->integer('type')->unsigned();
            $table->foreign('type')->references('id')->on('type_products');
            $table->float('value_type', 10, 2);
            $table->float('real_purchase_price', 10, 2);
            $table->float('last_purchase_price', 10, 2)->nullable();
            $table->float('quantity_default_value', 10, 2)->nullable();
            $table->float('stock', 10, 2)->nullable();
            $table->float('accumulator_value', 10, 2)->nullable();
            $table->dateTime('last_change_stock')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}