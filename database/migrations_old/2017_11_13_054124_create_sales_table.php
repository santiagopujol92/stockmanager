<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('id_person')->unsigned();
            // $table->foreign('id_person')->references('id')->on('people');
            $table->integer('id_person')->default(0);
            $table->integer('id_person_employee')->default(0);
            $table->float('cost_price');
            $table->float('sell_price');
            $table->timestamp('sell_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
