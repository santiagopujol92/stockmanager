<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodsBySalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods_by_sales', function (Blueprint $table) {
            $table->integer('id_sale')->unsigned();
            $table->foreign('id_sale')->references('id')->on('sales');
            $table->integer('id_food')->unsigned();
            $table->foreign('id_food')->references('id')->on('foods');
            $table->integer('quantity')->default(1);
            $table->timestamps();
            $table->softDeletes();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods_by_sales');
    }
}
