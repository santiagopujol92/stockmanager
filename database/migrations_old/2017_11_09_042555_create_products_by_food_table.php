<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsByFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_by_food', function (Blueprint $table) {
            $table->integer('id_food')->unsigned();
            $table->foreign('id_food')->references('id')->on('foods');
            $table->integer('id_product')->unsigned();
            $table->foreign('id_product')->references('id')->on('productos');
            $table->integer('quantity')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_by_food');
    }
}
