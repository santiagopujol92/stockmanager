<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('type_users')->insert([
        	'id' => 1,
            'description' => 'Admin'
        ]);
        DB::table('type_users')->insert([
            'id' => 2,
            'description' => 'Root'
        ]);
        DB::table('users')->insert([
            'name' => 'Admin',
            'lastname' => 'Admin',
            'email' => 'admin',
            'password' => bcrypt('asd123'),
            'type' => '1',
            'status' => 'on',
        ]);
        DB::table('users')->insert([
            'name' => 'Root',
            'lastname' => 'Root',
            'email' => 'root',
            'password' => bcrypt('talleres1'),
            'type' => '1',
            'status' => 'on',
        ]);
    }
}
