<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class FoodsBySale extends Model implements AuditableContract
{
    protected $table = 'foods_by_sale';
    protected $fillable = ['id_sale', 'id_food', 'quantity'];
    protected $dates = ['deleted_at'];
}
