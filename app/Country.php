<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Country extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'countries';
    protected $fillable = ['description'];
    protected $dates = ['deleted_at'];
}
