<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ProductsByFood extends Model implements AuditableContract
{
    protected $table = 'products_by_food';
    protected $fillable = ['id_food', 'id_product', 'quantity'];
    protected $dates = ['deleted_at'];
}
