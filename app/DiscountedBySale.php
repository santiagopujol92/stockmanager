<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class DiscountedBySale extends Model implements AuditableContract
{
    protected $table = 'discounted_by_sales';
    protected $fillable = ['id_sale', 'id_product', 'quantity_discounted'];
    protected $dates = ['deleted_at'];
}
