<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CondicionIva extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

	protected $table = 'condicion_ivas';
	protected $fillable = ['description'];
	protected $dates = ['deleted_at'];

}
