<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class City extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'cities';
    protected $fillable = ['description', 'id_province'];
    protected $dates = ['deleted_at'];
}