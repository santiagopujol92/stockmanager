<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Sale extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'sales';
    protected $fillable = ['id_person', 'cost_price', 'sell_price', 'sell_date', 'id_person_employee'];
    protected $dates = ['deleted_at'];
}
