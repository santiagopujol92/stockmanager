<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Tables extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'tables';
    protected $fillable = ['description', 'id_status_table', 'id_sale'];
    protected $dates = ['deleted_at'];
}
