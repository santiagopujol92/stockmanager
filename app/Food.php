<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Food extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'foods';
    protected $fillable = ['description', 'sell_price', 'cost_price', 'status', 'type'];
    protected $dates = ['deleted_at'];
}
