<?php

namespace StockManager\Http\Controllers;

use Illuminate\Http\Request;
use \StockManager\Food; 
use \StockManager\Producto; 
use \StockManager\ProductsByFood; 
use \StockManager\Sale; 
use \StockManager\Person; 
use \StockManager\DiscountedBySale; 
use Session;
use Redirect;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas
use \Carbon\Carbon;

class SaleController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        $this->middleware('auth');

        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'VENTA';
        $this->modulo_msg = 'Venta';
        $this->form = 'Venta';
        $this->module = 'ventas';
        $this->name_file = 'sale';
        $this->modals_btns = 'Sale';
        $this->model = new Sale;  
    }

    public function listing()
    {
        $data_controller = $this->model->join('people as p', 'sales.id_person', '=', 'p.id')
            ->select('sales.*', 'p.name as person_name', 'p.lastname as person_lastname')
            ->orderBy('sales.sell_date', 'desc')
            ->get();

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_clients = Person::select('name','lastname', 'id', 'cuit')
            ->where('id_type_people', '=', '1')
            ->orderBy('lastname', 'desc')
            ->get();

        $data_employees = Person::select('name','lastname', 'id', 'cuit')
            ->where('id_type_people', '=', '2')
            ->orderBy('lastname', 'desc')
            ->get();

        $data_food = Food::select('*')->where('status', '=', 1)->orderBy('type', 'desc')->get();
        $data_controller = $this->model->join('people as p', 'sales.id_person', '=', 'p.id')
            ->select('sales.*', 'p.name as person_name', 'p.lastname as person_lastname')
            ->orderBy('sales.id', 'asc')
            ->get();

        return view($this->module . '.' . $this->name_file . '_index', compact('data_controller', 'data_clients', 'data_food', 'data_employees'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . '_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            //formateo de fecha timestamp con datetime
            $carbon = new Carbon;
            if (isset($request->sell_date) && $request->sell_date != ''){
                $request['sell_date'] = $carbon->formatDateToSaveBD($request->sell_date, 'Y-m-d H:i:s');
            }

            //Creo la venta
            $inserted_sale = Sale::create($request->all());

            //Preparando datos e Insertando comidas de venta
            $result_foods = $this->insertFoodsBySales($request, $inserted_sale['id'], true);

            //Si la respuesta en la posicion status es identica a false significaq no se pudo crear las comidas por problema de stock
            //Se elimina la venta recien insertada y se responde los productos faltantes de stock
            if ($result_foods['status'] === false){
                $this->destroy($inserted_sale['id']);
                //Al responders_custom en el abm_basic hace particularidades de mensaje 
                return response()->json([
                    'response_custom' => 'stock_error',
                    'productos_sin_stock' => $result_foods['products_without_stock']
                ]);
            }

            //Respuesta por defecto si viene result_food en true o en null entra aca
            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creada Correctamente'
            ]);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Datos venta
        $data_controller = Sale::select('sales.*')
            ->orderBy('sales.id', 'asc')
            ->where('sales.id', '=', $id)
            ->get();

        //Comidas de venta
        $data_foods_by_sale = DB::table('foods_by_sales')
            ->select('*')
            ->orderBy('id_sale', 'asc')
            ->where('id_sale', '=', $id)
            ->get();

        //Return array con 2 array adentro
        return response()->json(
            array(
                'data_controller' => $data_controller->toArray(),
                'data_foods_by_sale' => $data_foods_by_sale->toArray()
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //formateo de fecha timestamp con datetime
        $carbon = new Carbon;
        if (isset($request->sell_date) && $request->sell_date != ''){
            $request['sell_date'] = $carbon->formatDateToSaveBD($request->sell_date, 'Y-m-d H:i:s');
        }


        //Si viene viene el campo cost_price significa que se ha desbloqueado entonces hacer la verificacion. sino no hacer nada de las comidas
        if (isset($request->cost_price)){
            //Preparando datos e Insertando comidas de venta
            $result_foods = $this->insertFoodsBySales($request, $id, true);

            //Si la respuesta en la posicion status es identica a false significaq no se pudo crear las comidas por problema de stock
            //Se elimina la venta recien insertada y se responde los productos faltantes de stock
            if ($result_foods['status'] === false){
                //Al responders_custom en el abm_basic hace particularidades de mensaje 
                return response()->json([
                    'response_custom' => 'stock_error',
                    'productos_sin_stock' => $result_foods['products_without_stock']
                ]);
            }
        }

        //Update de venta
        $data_controller = Sale::find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        //Respuesta por defecto si viene result_food en true o en null entra aca
        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificada Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Borrar todos las comidas de esta venta
        DB::table('foods_by_sales')->where('id_sale', '=', $id)->delete();
    
        //Aumentar el stock de productos que se desconto en esta venta consultando la tabla discounted_by_sales
        
        //Obtenemos los productos y cantidades descontadas
        $products_discounted = $this->getProductsDiscountedBySale($id);

        //Updateamos el stock en los productos sumandole la cantidad descontada a cada producto
        $result_stock_increment = $this->incrementStockOfProduct($id, $products_discounted);

        //Luego de aumentar el stock borramos los registros pertenecientes a esta venta en discounted_by_sales
        if ($result_stock_increment){
            DB::table('discounted_by_sales')->where('id_sale', '=', $id)->delete();
        }

        $data_controller = Sale::find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminada Correctamente'     
        ]);

    }

    //funcion que limpia e re inserta comidas de ventas
    public function insertFoodsBySales(Request $request, $id_sale, $verify_stock = false)
    {
        //Borrar todos las comidas de esta venta y asignarles las nuevos
        DB::table('foods_by_sales')->where('id_sale', '=', $id_sale)->delete();

        //Array para tabla foods by sale
        //id_food y quantity por posicion
        $foodsBySales = array();
        foreach ($request->all() as $key => $value) {

            //Si encutentro la key con check_food extraigo el numero que es el id de comida
            //y lo agrego en la posicion id_food en id_food al valor
            if (strpos($key, 'check_food') !== false){
                $arr_food = explode('-', $key);
                $id_food = $arr_food[1];
                $foodsBySales[$id_food]['id_food'] = $id_food;
            }

            //Busco las quantity comidas y extraigo el idfood y en la posicion del idfood en quantity
            //seteo el valor
            if (strpos($key, 'quantity_food') !== false){
                $arr_quantity = explode('-', $key);
                $id_food2 = $arr_quantity[1];
                $foodsBySales[$id_food2]['quantity'] = $value;
            }

        }

        //Funcionamiento de stock si se pasa el parametro para verificar en true SOLO
        if ($verify_stock){
            $result_stock = $this->verifyAndDiscountStock($foodsBySales, $id_sale);

            //Si se verifico y dio
            if ($result_stock['status'] == false){
                return $result_stock;
            }
        }

        //Luego de pasar las validaciones de stock y descontarse
        //Insertamos en foods_by_sale
        //Si el validador de stock esta en false entra derecho aca. (No checkea stock e inserta directo)
        foreach ($foodsBySales as $key => $value) {
            if (isset($value['id_food'])){
                DB::table('foods_by_sales')->insert(array(
                    array(
                        'id_sale' => $id_sale, 
                        'id_food' => $value['id_food'],
                        'quantity' => $value['quantity'],
                        'created_at' => date('Y-m-d H:i:m')
                    ))
                );
            }
        }

        return true;
    }

    //Verificador de Stock retorna un array con
    // estado: si vuelve como '' es porque no hay comidas seleccionadas en la venta y no se verifico ningun producto
            // si vuelve como true es porque se verifico y puede cargar la venta ya que el stock de los productos esta disponible
            // si vuelve como false es que no hay stock disponible para algunas de las comidas
    // productos_sin stock : si esto vuelve como array vacio es porq no hay productos sin stock
    //                    // si esto vuelve con productos son los productos que no tienen stock y se muestran como mensaje
    function verifyAndDiscountStock($foodsBySales, $id_sale)
    {
        $products_without_stock = array();
        $puedeDescontarStock = '';
        $totalQuantityProductToDiscount = array();

        //Verificamos si puede descontar stock revisando todas las comidas con la cantidad y sus productos de cada comida
        //Obtener el array de nueva venta creada y con el id
        //guardar en la tabla FoodsBySale los valores del array $foodsBySales
        //Recorrido de cada comida
        foreach ($foodsBySales as $key => $food) 
        {
            //Si el id de comida tiene valor
            if (isset($food['id_food'])){

                //Obtener los productos y la cantidad de uso que tiene ese producto en esta comida

                $products_of_this_food = DB::table('products_by_food')
                    ->join('productos as p', 'p.id', '=', 'products_by_food.id_product')
                    ->select('products_by_food.id_product', 'products_by_food.quantity', 'p.stock', 'p.description')
                    ->where('products_by_food.id_food', '=', $food['id_food'])
                    ->get();

                //Tenemos array de cada producto utilizado en esta comida(id_producto, cantidad usada, stock disponible)
                $products_of_this_food = $products_of_this_food->toArray();
                $puedeDescontarStock = true;

                //Recorrer array de productos de esta comida
                foreach ($products_of_this_food as $i => $product) {
                    //Calculamos la cantidad que tiene de producto asignada la comida multiplicando por la cantidad de comidas
                    $total_quantity_product_by_food_of_sale = (float)$food['quantity'] * (float)$product->quantity;

                    //Guardamos la cantidad total del producto usada, el stock y el id_product en un array para luego recorrerlo y descontar
                    $totalQuantityProductToDiscount[$i]['total_quantity_product_by_food_of_sale'] = $total_quantity_product_by_food_of_sale;
                    $totalQuantityProductToDiscount[$i]['stock'] = $product->stock;
                    $totalQuantityProductToDiscount[$i]['id_product'] = $product->id_product;

                    //Verificar si el stock disponible de ese producto es mayor a la cantidad usada de ese en esta comida. Si es menor entra al if y empieza a guardar los productos sin stock
                    if ($product->stock < $total_quantity_product_by_food_of_sale){
                        //A penas uno se cumpla ya no se permite crear la venta y se pone el descuento en falseo
                        //Por el otro lado guardamos los productos sin stock para devolverlos al mostar

                        //Verificar si el producto este ya fue asignado desde otra comida
                        if (array_search($product->description, $products_without_stock) === false){
                            $products_without_stock[] = $product->description;
                        }
                        $puedeDescontarStock = false;
                    }
                }
            }    
        }

        //Si puede descontar stock
        if ($puedeDescontarStock){
            //Si el stock es mayor a lo que se usa entonces 
            //hacer un update al stock de ese producto de esta comida 
            //restandole lo que se usa en esta comidas

            //Recorremos el vector que tiene los productos y su descuento
            if (count($totalQuantityProductToDiscount) > 0){
                foreach ($totalQuantityProductToDiscount as $product) {

                    //restamos stock
                    $descuento_stock = (float)$product['stock'] - (float)$product['total_quantity_product_by_food_of_sale'];

                    //Actualizamos Stock descontando
                    DB::table('productos')
                        ->where('id', $product['id_product'])
                        ->update(['stock' => $descuento_stock]
                    );

                    //Insertamos en descuentos de producto por venta para tener un registro de lo restado.
                    //Lo cual nos sirve para acumular stock al eliminar una venta que se de de baja.
                    DB::table('discounted_by_sales')->insert(array(
                    array(
                        'id_sale' => $id_sale, 
                        'id_product' => $product['id_product'],
                        'quantity_discounted' => (float)$product['total_quantity_product_by_food_of_sale'],
                        'created_at' => date('Y-m-d H:i:m')
                    ))
                );
                }
            }
        }

        //Retorno guardamos valores en el array 
        //y devolvemos el stado depende si desconto o no y los productos sin stock
        //en todas las comidas
        return array(
            'status' => $puedeDescontarStock,
            'products_without_stock' => $products_without_stock
        );
    }

    //Obtenemos los productos y cantidades descontadas de cierta venta 
    //Devuelve array de los productos y la cantidad
    function getProductsDiscountedBySale($id_sale){

        $products_discounted = DB::table('discounted_by_sales')
            ->join('productos as p', 'p.id', '=', 'discounted_by_sales.id_product')
            ->select('discounted_by_sales.id_product', 'discounted_by_sales.quantity_discounted', 'p.stock')
            ->where('id_sale', '=', $id_sale)
            ->get();

        $products_discounted = $products_discounted->toArray();
        return $products_discounted;
    }

    //Funcion que incrementa el stock de productos de acuerdo a la cantidad que trae el vector en cada producto
    //Devuelve boolean
    function incrementStockOfProduct($id_sale, $products_discounted){
        $retorno = false;

        if (count($products_discounted) > 0){
            foreach ($products_discounted as $product) {

                $increment_stock = (float)$product->stock + (float)$product->quantity_discounted;
                
                DB::table('productos')
                    ->where('id', $product->id_product)
                    ->update(['stock' => $increment_stock]
                );
                $retorno = true;
            }
        }
        return $retorno;
    }

    /**
     * Eliminar comidas y bebidas de la venta sin modificar la venta
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFoodsBySale($id)
    {
        //Borrar todos las comidas de esta venta
        DB::table('foods_by_sales')->where('id_sale', '=', $id)->delete();
    
        //Aumentar el stock de productos que se desconto en esta venta consultando la tabla discounted_by_sales
        
        //Obtenemos los productos y cantidades descontadas
        $products_discounted = $this->getProductsDiscountedBySale($id);

        //Updateamos el stock en los productos sumandole la cantidad descontada a cada producto
        $result_stock_increment = $this->incrementStockOfProduct($id, $products_discounted);

        //Luego de aumentar el stock borramos los registros pertenecientes a esta venta en discounted_by_sales
        if ($result_stock_increment){
            DB::table('discounted_by_sales')->where('id_sale', '=', $id)->delete();
        }

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Comidas y Bebidas de la venta Eliminadas Correctamente'     
        ]);

    }

    /**
     * Get all sales
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllSales()
    {
        $data_sales = Sale::join('people as p', 'sales.id_person_employee', '=', 'p.id')
            ->select('sales.id','sales.sell_date','p.name as employee_name', 'p.lastname as employee_last_name', 'sales.sell_price')
            ->orderBy('sell_date', 'desc')
            ->get();

        return response()->json(
            $data_sales->toArray()
        );
    }
}

