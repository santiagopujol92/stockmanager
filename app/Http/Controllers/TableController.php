<?php

namespace StockManager\Http\Controllers;

use Illuminate\Http\Request;
use \StockManager\Tables; 
use \StockManager\StatusTables; 
use \StockManager\Food; 
use \StockManager\Producto; 
use \StockManager\ProductsByFood; 
use \StockManager\Sale; 
use \StockManager\Person; 
use \StockManager\DiscountedBySale; 
use \StockManager\OrderByTable; 
use Session;
use Redirect;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas
use \Carbon\Carbon;


//Este controllador extiende de sale controller
class TableController extends SaleController
{
   /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        $this->middleware('auth');

        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'MESA';
        $this->modulo_msg = 'Mesa';
        $this->form = 'Mesa';
        $this->module = 'mesas';
        $this->name_file = 'table';
        $this->modals_btns = 'Table';
        $this->model = new Tables;  
    }

    public function listing()
    {
        $data_controller = $this->model->join('status_tables as st', 'tables.id_status_table', '=', 'st.id')
            ->select('tables.id', 'tables.description', 'tables.id_status_table', 'st.description as status')
            ->orderBy('tables.description', 'asc')
            ->get();

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_controller = $this->model->join('status_tables as st', 'tables.id_status_table', '=', 'st.id')
            ->select('tables.id', 'tables.description', 'tables.id_status_table', 'st.description as status')
            ->orderBy('tables.description', 'asc')
            ->get();

        $data_clients = Person::select('name','lastname', 'id', 'cuit')
            ->where('id_type_people', '=', '1')
            ->orderBy('lastname', 'desc')
            ->get();

        $data_employees = Person::select('name','lastname', 'id', 'cuit')
            ->where('id_type_people', '=', '2')
            ->orderBy('lastname', 'desc')
            ->get();

        $data_sales = Sale::join('people as p', 'sales.id_person_employee', '=', 'p.id')
            ->select('sales.id','sales.sell_date','p.name as employee_name', 'p.lastname as employee_last_name', 'sales.sell_price')
            ->orderBy('sell_date', 'desc')
            ->get();

        $data_food = Food::select('*')->where('status', '=', 1)->orderBy('type', 'desc')->get();

        $data_status_tables = StatusTables::All();

        return view($this->module . '.' . $this->name_file . 's_index', compact('data_controller', 'data_status_tables', 'data_clients', 'data_employees', 'data_food', 'data_sales'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . '_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'     
        ]);

    }

    // //Cambiar
    // function findByTypeProductId($id){
    //     $products = $this->model->join('type_products as tp', 'productos.type', '=', 'tp.id')
    //         ->select('productos.id', 'productos.description')
    //         ->where('productos.type', '=', $id)
    //         ->get();

    //     return response()->json(
    //         $products->toArray()
    //     );
    // }

    /**
     * Create the order by table 
     *
     * @return \Illuminate\Http\Response
     */
    public function createOrderByTable(Request $request){
        if($request->ajax())
        {
            $order_by_table = new OrderByTable;
            $order_by_table->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

}
