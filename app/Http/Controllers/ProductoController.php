<?php

namespace StockManager\Http\Controllers;

use Illuminate\Http\Request;
use \StockManager\Producto; 
use \StockManager\TypeProduct; 
use Session;
use Redirect;

class ProductoController extends Controller
{

    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        $this->middleware('auth');

        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'PRODUCTO';
        $this->modulo_msg = 'Producto';
        $this->form = 'Producto';
        $this->module = 'productos';
        $this->name_file = 'product';
        $this->modals_btns = 'Product';
        $this->model = new Producto;  
    }

    public function listing(){
        $data_controller = $this->model->join('type_products', 'productos.type', '=', 'type_products.id')
            ->select('productos.id', 
                'productos.description',
                'productos.value_type', 
                'productos.real_purchase_price',
                'productos.last_purchase_price',
                'productos.quantity_default_value',
                'productos.accumulator_value',
                'productos.type', 
                'productos.stock',
                'type_products.description as type_description')
            ->orderBy('productos.id', 'asc')
            ->get();
        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_type_products = TypeProduct::All();

        $data_controller = $this->model->join('type_products', 'productos.type', '=', 'type_products.id')
            ->select('productos.id', 
                'productos.description',
                'productos.value_type', 
                'productos.real_purchase_price',
                'productos.last_purchase_price',
                'productos.quantity_default_value',
                'productos.accumulator_value',
                'productos.type', 
                'productos.stock',
                'type_products.description as type_description')
            ->orderBy('productos.id', 'asc')
            ->get();
        return view($this->module . '.' . $this->name_file . 's_index', compact('data_controller', 'data_type_products'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'     
        ]);

    }
    
    //Buscar producto por tipo de producto
    function findByTypeProductId($id){
        $products = $this->model->join('type_products as tp', 'productos.type', '=', 'tp.id')
            ->select('productos.id', 'productos.description')
            ->where('productos.type', '=', $id)
            ->get();

        return response()->json(
            $products->toArray()
        );
    }
}
