<?php

namespace StockManager\Http\Controllers;

use Illuminate\Http\Request;
use \StockManager\Food; 
use \StockManager\Producto; 
use \StockManager\ProductsByFood; 
use Session;
use Redirect;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas

class FoodController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        $this->middleware('auth');

        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'COMIDA O BEBIDA';
        $this->modulo_msg = 'Comida o Bebida';
        $this->form = 'Comida';
        $this->module = 'comidas';
        $this->name_file = 'food';
        $this->modals_btns = 'Food';
        $this->model = new Food;  
    }

    public function listing(){
        $data_controller = $this->model->select('foods.*')
            ->orderBy('foods.id', 'asc')
            ->get();
        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	$data_products = Producto::join('type_products', 'productos.type', '=', 'type_products.id')
            ->select('productos.id', 
                'productos.description',
                'productos.quantity_default_value',
                'productos.value_type',
                'productos.type',
                'productos.real_purchase_price',
                'productos.accumulator_value',
                'type_products.description as type_description')
            ->orderBy('productos.description', 'asc')
            ->get();

        $data_controller = $this->model->select('foods.*')
            ->orderBy('foods.id', 'asc')
            ->get();
        return view($this->module . '.' . $this->name_file . '_index', compact('data_controller', 'data_products'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . '_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->ajax())
        {
            //Creo la comida
            $inserted_food = $this->model->create($request->all());

            //Preparando datos e Insertando productos de comida
            $this->insertProductsByfood($request, $inserted_food['id']);

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creada Correctamente'
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Datos comida
        $data_controller = $this->model->select('foods.*')
            ->orderBy('foods.id', 'asc')
            ->where('foods.id', '=', $id)
            ->get();

        //Productos de comida
        $data_products_by_food = DB::table('products_by_food')
            ->select('*')
            ->orderBy('id_food', 'asc')
            ->where('id_food', '=', $id)
            ->get();

        //Return array con 2 array adentro
        return response()->json(
            array(
                'data_controller' => $data_controller->toArray(),
                'data_products_by_food' => $data_products_by_food->toArray()
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Update de comida
        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        //Preparando datos e Insertando productos de comida
        $this->insertProductsByfood($request, $id);

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificada Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Borrar todos los productos de esta comida
        DB::table('products_by_food')->where('id_food', '=', $id)->delete();

        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminada Correctamente'     
        ]);

    }

    public function insertProductsByfood(Request $request, $id_food)
    {
        //Borrar todos los productos de esta comida y asignarles los nuevos
        DB::table('products_by_food')->where('id_food', '=', $id_food)->delete();

        //Array para tabla products by food
        //id_product y quantity por posicion
        $productsByFood = array();
        foreach ($request->all() as $key => $value) {

            //Si encutentro la key con check_product extraigo el numero que es el id de producto
            //y lo agrego en la posicion id_prducto en id_producto al valor
            if (strpos($key, 'check_product') !== false){
                $arr_product = explode('-', $key);
                $id_product = $arr_product[1];
                $productsByFood[$id_product]['id_product'] = $id_product;
            }

            //Busco las quantity productos y extraigo el idproducto y en la posicion del idproducto en quantity
            //seteo el valor
            if (strpos($key, 'quantity_product') !== false){
                $arr_quantity = explode('-', $key);
                $id_product2 = $arr_quantity[1];
                $productsByFood[$id_product2]['quantity'] = $value;
            }

        }

        //Obtener el array de nueva comida creada y con el id
        //guardar en la tabla ProdcutsByFood los valores del array $productsByFood
        foreach ($productsByFood as $key => $value) {
            //Insertamos en productsbyfood
            if (isset($value['id_product'])){
                DB::table('products_by_food')->insert(array(
                    array(
                        'id_food' => $id_food, 
                        'id_product' => $value['id_product'],
                        'quantity' => $value['quantity'],
                        'created_at' => date('Y-m-d H:i:m')
                    ))
                );
            }
        }
    }
}
