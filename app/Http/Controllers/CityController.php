<?php

namespace StockManager\Http\Controllers;

use Illuminate\Http\Request;
use \StockManager\City; 
use \StockManager\Province; 
use \StockManager\Country; 
use Session;
use Redirect;

class CityController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        $this->middleware('auth');

        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'CIUDAD';
        $this->modulo_msg = 'Ciudad';
        $this->form = 'Ciudad';
        $this->module = 'ciudades';
        $this->name_file = 'citie';
        $this->modals_btns = 'Citie';
        $this->model = new City;
    }

    public function listing(){
        $data_controller = $this->model->join('provinces as p', 'cities.id_province', '=', 'p.id')
            ->join('countries as c', 'p.id_country', '=', 'c.id')
            ->select('cities.*', 'p.description as province_name', 'c.description as country_name')
            ->orderBy('cities.id', 'asc')
            ->get();

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $country = new Country();
        $data_countries = $country->All();        

        $data_controller = $this->model->join('provinces as p', 'cities.id_province', '=', 'p.id')
            ->join('countries as c', 'p.id_country', '=', 'c.id')
            ->select('cities.*', 'p.description as province_name', 'c.description as country_name')
            ->orderBy('cities.id', 'asc')
            ->get();

        return view($this->module . '.' . $this->name_file . 's_index', compact('data_controller', 'data_countries'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->join('provinces as p', 'cities.id_province', '=', 'p.id')
            ->join('countries as c', 'p.id_country', '=', 'c.id')
            ->select('cities.*', 'p.description as province_name', 'c.description as country_name', 'c.id as id_country')
            ->where('cities.id', '=', $id) 
            ->get();

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'     
        ]);

    }

    /**
     * Retorna las ciudades filtradas por id de provincia
     */
    public function findByProvinceId($id){

        $cities = $this->model->join('provinces as p', 'cities.id_province', '=', 'p.id')
            ->select('cities.id', 'cities.description')
            ->where('cities.id_province', '=', $id)
            ->get();

        return response()->json(
            $cities->toArray()
        );
    }
}
