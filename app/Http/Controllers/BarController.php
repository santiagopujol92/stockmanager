<?php

namespace StockManager\Http\Controllers;

use Illuminate\Http\Request;
use \StockManager\Tables; 
use \StockManager\StatusTables; 
use \StockManager\OrderByTable;
use \StockManager\StatusOrderByTable; 
use Session;
use Redirect;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas
use \Carbon\Carbon;

class BarController extends Controller
{
   /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        $this->middleware('auth');

        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'BAR';
        $this->modulo_msg = 'Bar';
        $this->form = 'Bar';
        $this->module = 'bar';
        $this->name_file = 'bar';
        $this->modals_btns = 'Bar';
        $this->model = new OrderByTable;  
    }

    // public function listing()
    // {
    //     $data_tables_ocupated = $this->model->join('status_tables as st', 'tables.id_status_table', '=', 'st.id')
    //         ->select('tables.id', 'tables.description', 'tables.id_status_table', 'st.description as status')
    //         ->orderBy('tables.description', 'asc')
    //         ->get();

    //     return response()->json(
    //         $data_controller->toArray()
    //     );
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //PARA ORDENES DE COMIDAS A PREPARAR
        $data_tables_with_orders_products_1 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
        	->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
        	->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 't.id as id_tabla', 't.description as nro_mesa', 'fbs.id_food', 'fbs.quantity as cantidad', 'f.description as nombre_producto', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '1')
            ->where('f.type', '=', 'Bebida')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '<', [Carbon::today()])
            ->get();

        $data_tables_with_orders_1 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 'order_by_tables.id_table as id_tabla', 't.description as nro_mesa', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '1')
            ->where('f.type', '=', 'Bebida')
            ->groupBy('order_by_tables.id', 'order_by_tables.created_at', 'order_by_tables.id_table', 't.description')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '>=', [Carbon::yesterday()])
            ->get();

        //PARA ORDENES DE COMIDAS A RETIRAR
        $data_tables_with_orders_products_2 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 't.id as id_tabla', 't.description as nro_mesa', 'fbs.id_food', 'fbs.quantity as cantidad', 'f.description as nombre_producto', 'order_by_tables.created_at')
            ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '2')
            ->where('f.type', '=', 'Bebida')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '<', [Carbon::today()])
            ->get();

        $data_tables_with_orders_2 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id_table as id_tabla', 't.description as nro_mesa', 'order_by_tables.created_at')
            ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '2')
            ->where('f.type', '=', 'Bebida')
            ->groupBy('order_by_tables.created_at', 'order_by_tables.id_table', 't.description')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '>=', [Carbon::yesterday()])
            ->get();

        //PARA ORDENES DE COMIDAS ENTREGADAS
        $data_tables_with_orders_products_3 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 't.id as id_tabla', 't.description as nro_mesa', 'fbs.id_food', 'fbs.quantity as cantidad', 'f.description as nombre_producto', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '3')
            ->where('f.type', '=', 'Bebida')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '<', [Carbon::today()])
            ->get();

        $data_tables_with_orders_3 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id_table as id_tabla', 't.description as nro_mesa', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '3')
            ->where('f.type', '=', 'Bebida')
            ->groupBy('order_by_tables.created_at', 'order_by_tables.id_table', 't.description')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '>=', [Carbon::yesterday()])
            ->get();
        //PARA ORDENES DE COMIDAS CANCELADAS
        $data_tables_with_orders_products_4 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 't.id as id_tabla', 't.description as nro_mesa', 'fbs.id_food', 'fbs.quantity as cantidad', 'f.description as nombre_producto', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '4')
            ->where('f.type', '=', 'Bebida')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '<', [Carbon::today()])
            ->get();

        $data_tables_with_orders_4 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id_table as id_tabla', 't.description as nro_mesa', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '4')
            ->where('f.type', '=', 'Bebida')
            ->groupBy('order_by_tables.created_at', 'order_by_tables.id_table', 't.description')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '>=', [Carbon::yesterday()])
            ->get();


        $data_status_order_by_tables = StatusOrderByTable::select('id','description')
            ->orderBy('description', 'desc')
            ->get();


        //Agrupar bien la data y devolverla
        return view($this->module . '.' . $this->name_file . '_index', compact('data_tables_ocupated', 'data_tables_with_orders_1', 'data_tables_with_orders_products_1', 'data_tables_with_orders_products_2', 'data_tables_with_orders_2', 'data_tables_with_orders_3', 'data_tables_with_orders_products_3', 'data_tables_with_orders_products_4', 'data_tables_with_orders_4', 'data_status_order_by_tables' ))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    public function listing()
    {
        //PARA ORDENES DE COMIDAS A PREPARAR
        $data_tables_with_orders_products_1 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 't.id as id_tabla', 't.description as nro_mesa', 'fbs.id_food', 'fbs.quantity as cantidad', 'f.description as nombre_producto', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '1')
            ->where('f.type', '=', 'Bebida')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '<', [Carbon::today()])
            ->get();

        $data_tables_with_orders_1 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 'order_by_tables.id_table as id_tabla', 't.description as nro_mesa', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '1')
            ->where('f.type', '=', 'Bebida')
            ->groupBy('order_by_tables.id', 'order_by_tables.created_at', 'order_by_tables.id_table', 't.description')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '>=', [Carbon::yesterday()])
            ->get();

        //PARA ORDENES DE COMIDAS A RETIRAR
        $data_tables_with_orders_products_2 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 't.id as id_tabla', 't.description as nro_mesa', 'fbs.id_food', 'fbs.quantity as cantidad', 'f.description as nombre_producto', 'order_by_tables.created_at')
            ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '2')
            ->where('f.type', '=', 'Bebida')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '<', [Carbon::today()])
            ->get();

        $data_tables_with_orders_2 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id_table as id_tabla', 't.description as nro_mesa', 'order_by_tables.created_at')
            ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '2')
            ->where('f.type', '=', 'Bebida')
            ->groupBy('order_by_tables.created_at', 'order_by_tables.id_table', 't.description')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '>=', [Carbon::yesterday()])
            ->get();

        //PARA ORDENES DE COMIDAS ENTREGADAS
        $data_tables_with_orders_products_3 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 't.id as id_tabla', 't.description as nro_mesa', 'fbs.id_food', 'fbs.quantity as cantidad', 'f.description as nombre_producto', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '3')
            ->where('f.type', '=', 'Bebida')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '<', [Carbon::today()])
            ->get();

        $data_tables_with_orders_3 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id_table as id_tabla', 't.description as nro_mesa', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '3')
            ->where('f.type', '=', 'Bebida')
            ->groupBy('order_by_tables.created_at', 'order_by_tables.id_table', 't.description')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '>=', [Carbon::yesterday()])
            ->get();
        //PARA ORDENES DE COMIDAS CANCELADAS
        $data_tables_with_orders_products_4 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id', 't.id as id_tabla', 't.description as nro_mesa', 'fbs.id_food', 'fbs.quantity as cantidad', 'f.description as nombre_producto', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '4')
            ->where('f.type', '=', 'Bebida')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '<', [Carbon::today()])
            ->get();

        $data_tables_with_orders_4 = $this->model->join('tables as t', 't.id', '=', 'order_by_tables.id_table')
            ->join('foods_by_sales as fbs', 'fbs.id_sale', '=', 'order_by_tables.id_sale')
            ->join('foods as f', 'f.id', '=', 'fbs.id_food')
            ->select('order_by_tables.id_table as id_tabla', 't.description as nro_mesa', 'order_by_tables.created_at')
            // ->where('t.id_status_table', '=', '1')
            ->where('order_by_tables.id_status_order_by_table', '=', '4')
            ->where('f.type', '=', 'Bebida')
            ->groupBy('order_by_tables.created_at', 'order_by_tables.id_table', 't.description')
            ->orderBy('order_by_tables.created_at', 'desc')
            ->orderBy('t.description', 'asc')
            // ->where('order_by_tables.created_at', '>=', [Carbon::yesterday()])
            ->get();

        return response()->json(
            compact('data_tables_ocupated', 'data_tables_with_orders_1', 'data_tables_with_orders_products_1', 'data_tables_with_orders_products_2', 'data_tables_with_orders_2', 'data_tables_with_orders_3', 'data_tables_with_orders_products_3', 'data_tables_with_orders_products_4', 'data_tables_with_orders_4')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }


}
