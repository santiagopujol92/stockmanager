<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Producto extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'productos';
    protected $fillable = ['description', 'type', 'value_type', 'real_purchase_price', 'last_purchase_price', 'quantity_default_value', 'stock', 'accumulator_value', 'last_change_stock'];
    protected $dates = ['deleted_at'];

}
