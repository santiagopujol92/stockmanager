<?php

namespace StockManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class OrderByTable extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'order_by_tables';
    protected $fillable = ['id_status_order_by_table', 'id_table', 'id_sale'];
    protected $dates = ['deleted_at'];
}
